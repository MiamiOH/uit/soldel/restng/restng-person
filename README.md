# Person RESTng Web Service
## Docker Setup
1. Follow the instructions here: https://wstest.apps.miamioh.edu/api/docs/docker/projects/. 
    * Your swagger url will be https://localhost/swagger-ui/ if you use the docker set up from the instructions.
2. On the restng docker container change to the `/var/www/restng/config` directory.
3. Create the following files:
    * autoloader.yaml
    * datasources.yaml
    * providers.yaml 
    * services.yaml
### Autoloader File
    ---
    autoload:
      - /var/www/restng/service/person/vendor/autoload.php
### Datasource File
    ---
    MUWS_SEC_PROD:
      type: OCI8
      user: MUWS_SEC
      password: 
      database: DEVL
      port:
      connect_type:
      host:
 ### Providers File
    ---
    resources:
      - /var/www/restng/service/person/resources.php
### Services File
    ---
    services:
      MU\BannerIdFactory:
        class: \MiamiOH\RESTng\Service\Extension\BannerIdFactory
        description: Provides Banner ID object factory.
        set:
          database:
            type: service
            name: APIDatabaseFactory
      MU\BannerUtil:
        class: \MiamiOH\RESTng\Service\Extension\BannerUtil
        description: Provides utility functions for working with Banner data.
        set:
          bannerIdFactory:
            type: service
            name: MU\BannerIdFactory

    