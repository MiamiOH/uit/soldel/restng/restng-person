<?php

namespace MiamiOH\RestngPersonWebService\Repositories;

use MiamiOH\RestngPersonWebService\Requests\AdditionalIdentificationRequest;

interface AdditionalIdentificationRepository
{
    public function create(AdditionalIdentificationRequest $request): bool;
}
