<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 01/29/19
 * Time: 5:12 PM
 */

namespace MiamiOH\RestngPersonWebService\Repositories;

use MiamiOH\RestngPersonWebService\EloquentModels\PersonsModel;
use MiamiOH\RestngPersonWebService\Objects\Person;

class PersonRepositorySQL implements PersonRepository
{

    /**
     * @param Person $person
     * @return array
     */
    public function create(Person $person): bool
    {
        PersonsModel::create([
            'spbpers_pidm' => $person->getPidm(),
            'spbpers_ssn' => $person->getSsn(),
            'spbpers_birth_date' => $person->getBirthDate(),
            'spbpers_lgcy_code' => $person->getLegacyCode(),
            'spbpers_ethn_code' => $person->getEthnicCode(),
            'spbpers_mrtl_code' => $person->getMaritalCode(),
            'spbpers_sex' => $person->getSex(),
            'spbpers_activity_date' => $person->getActivityDate()->format('Y-m-d H:i:s'),
            'spbpers_vetc_file_number' => $person->getVeteranIdNumber(),
            'spbpers_legal_name' => $person->getLegalName(),
            'spbpers_pref_first_name' => $person->getPreferredFirstName(),
            'spbpers_name_prefix' => $person->getNamePrefix(),
            'spbpers_name_suffix' => $person->getNameSuffix(),
            'spbpers_ethn_cde' => $person->getGovernmentEthnicCode(),
            'spbpers_confid_ind' => $person->getConfidentialIndicator(),
            'spbpers_dead_ind' => $person->getDeceasedIndicator(),
            'spbpers_dead_date' => $person->getDeceasedDate(),
            'spbpers_vera_ind' => $person->getVeteranIndicator(),
            'spbpers_citz_ind' => $person->getCitizenIndicator(),
            'spbpers_armed_serv_med_vet_ind' => $person->getArmedForcesIndicator(),
            'spbpers_gndr_code' => $person->getGenderCode(),
            'spbpers_pprn_code' => $person->getPersonalPronoun(),
            'spbpers_user_id' => $person->getUserId(),
            'spbpers_data_origin' => $person->getDataOrigin(),
        ]);

        return true;
    }

    /**
     * @param Person $person
     * @return array
     */
    public function update(Person $person): bool
    {
        $personModel = PersonsModel::where('spbpers_pidm', $person->getPidm())->first();

        $modifiedAttributes = $person->getModifiedAttributes();

        if (isset($modifiedAttributes['ssn'])) {
            $personModel->spbpers_ssn = $person->getSsn();
        };

        if (isset($modifiedAttributes['legacyCode'])) {
            $personModel->spbpers_lgcy_code = $person->getLegacyCode();
        };

        if (isset($modifiedAttributes['ethnicCode'])) {
            $personModel->spbpers_ethn_code = $person->getEthnicCode();
        };

        if (isset($modifiedAttributes['maritalCode'])) {
            $personModel->spbpers_mrtl_code = $person->getMaritalCode();
        };

        if (isset($modifiedAttributes['sex'])) {
            $personModel->spbpers_sex = $person->getSex();
        };

        if (isset($modifiedAttributes['veteranIdNumber'])) {
            $personModel->spbpers_vetc_file_number = $person->getVeteranIdNumber();
        };

        if (isset($modifiedAttributes['confidentialIndicator'])) {
            $personModel->spbpers_confid_ind = $person->getConfidentialIndicator();
        };

        if (isset($modifiedAttributes['deceasedIndicator'])) {
            $personModel->spbpers_dead_ind = $person->getDeceasedIndicator();
        };

        if (isset($modifiedAttributes['deceasedDate'])) {
            $personModel->spbpers_dead_date = $person->getDeceasedDate();
        };

        if (isset($modifiedAttributes['veteranIndicator'])) {
            $personModel->spbpers_vera_ind = $person->getVeteranIndicator();
        };

        if (isset($modifiedAttributes['citizenIndicator'])) {
            $personModel->spbpers_citz_ind = $person->getCitizenIndicator();
        };

        if (isset($modifiedAttributes['armedForcesIndicator'])) {
            $personModel->spbpers_armed_serv_med_vet_ind = $person->getArmedForcesIndicator();
        };

        if (isset($modifiedAttributes['birthDate'])) {
            $personModel->spbpers_birth_date = $person->getBirthDate();
        };

        if (isset($modifiedAttributes['legalName'])) {
            $personModel->spbpers_legal_name = $person->getLegalName();
        };

        if (isset($modifiedAttributes['preferredFirstName'])) {
            $personModel->spbpers_pref_first_name = $person->getPreferredFirstName();
        };

        if (isset($modifiedAttributes['namePrefix'])) {
            $personModel->spbpers_name_prefix = $person->getNamePrefix();
        };

        if (isset($modifiedAttributes['nameSuffix'])) {
            $personModel->spbpers_name_suffix = $person->getNameSuffix();
        };

        if (isset($modifiedAttributes['governmentEthnicCode'])) {
            $personModel->spbpers_ethn_cde = $person->getGovernmentEthnicCode();
        };

        if (isset($modifiedAttributes['genderCode'])) {
            $personModel->spbpers_gndr_code = $person->getGenderCode();
        };

        if (isset($modifiedAttributes['personalPronoun'])) {
            $personModel->spbpers_pprn_code = $person->getPersonalPronoun();
        };

        $personModel->spbpers_activity_date = $person->getActivityDate()->format('Y-m-d H:i:s');
        $personModel->spbpers_user_id = $person->getUserId();
        $personModel->spbpers_data_origin = $person->getDataOrigin();

        $personModel->save();

        return true;

    }

}