<?php

namespace MiamiOH\RestngPersonWebService\Repositories;

use Exception;
use MiamiOH\RestngPersonWebService\EloquentModels\AdditionalIdentificationModel;
use MiamiOH\RestngPersonWebService\Requests\AdditionalIdentificationRequest;

class AdditionalIdentificationRepositorySQL implements AdditionalIdentificationRepository
{

    /**
     * @param AdditionalIdentificationRequest $request
     * @return bool
     * @throws \Exception
     */
    public function create(AdditionalIdentificationRequest $request): bool
    {
            AdditionalIdentificationModel::create([
                'goradid_pidm' => $request->getPidm(),
                'goradid_adid_code' => $request->getAdditionalIdCode(),
                'goradid_additional_id' => $request->getAdditionalId(),
                'goradid_user_id' => $request->getuserId(),
                'goradid_data_origin' => $request->getDataOrigin(),
                'goradid_activity_date' => $request->getActivityDate()
            ]);

        return true;
    }
}
