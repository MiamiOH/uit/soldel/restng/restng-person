<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 01/29/19
 * Time: 4:32 PM
 */

namespace MiamiOH\RestngPersonWebService\Repositories;

use MiamiOH\RestngPersonWebService\Objects\Person;

interface PersonRepository
{
    public function create(Person $person): bool;

    public function update(Person $person): bool;

}