<?php

/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 01/29/19
 * Time: 5:12 PM
 */

namespace MiamiOH\RestngPersonWebService\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use MiamiOH\RestngPersonWebService\Collections\AdditionalIdentificationCollection;
use MiamiOH\RestngPersonWebService\EloquentModels\IdentificationModel;
use MiamiOH\RestngPersonWebService\Objects\AdditionalIdentification;
use MiamiOH\RestngPersonWebService\Objects\Identification;

class IdentificationRepositorySQL implements IdentificationRepository
{

    /**
     * @param Identification $identification
     * @return bool
     * @throws \Exception
     */
    public function create(Identification $identification): bool
    {
        try {
            IdentificationModel::where('spriden_pidm', $identification->getPidm())->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new ModelNotFoundException('PIDM not found.');
        }

        IdentificationModel::create([
            'spriden_pidm' => $identification->getPidm(),
            'spriden_id' => $identification->getMuid(),
            'spriden_last_name' => $identification->getLastName(),
            'spriden_first_name' => $identification->getFirstName(),
            'spriden_mi' => $identification->getMiddleName(),
            'spriden_change_ind' => $identification->getChangeIndicator(),
            'spriden_entity_ind' => $identification->getEntityIndicator(),
            'spriden_activity_date' => $identification->getActivityDate()->format('Y-m-d H:i:s'),
            'spriden_user_id' => $identification->getUserId(),
            'spriden_data_origin' => $identification->getDataOrigin(),
        ]);

        return true;
    }

    /**
     * @param Identification $identification
     * @return array
     * @throws \Exception
     */
    public function update(Identification $identification): bool
    {
        try {
            $identificationModel = IdentificationModel::where('spriden_surrogate_id', $identification->getSurrogateId())->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new ModelNotFoundException('Surrogate not found.');
        }

        $modifiedAttributes = $identification->getModifiedAttributes();

        if (isset($modifiedAttributes['pidm'])) {
            $identificationModel->spriden_pidm = $identification->getPidm();
        };

        if (isset($modifiedAttributes['muid'])) {
            $identificationModel->spriden_id = $identification->getMuid();
        };

        if (isset($modifiedAttributes['lastName'])) {
            $identificationModel->spriden_last_name = $identification->getLastName();
        };

        if (isset($modifiedAttributes['firstName'])) {
            $identificationModel->spriden_first_name = $identification->getFirstName();
        };

        if (isset($modifiedAttributes['middleName'])) {
            $identificationModel->spriden_mi = $identification->getMiddleName();
        };

        if (isset($modifiedAttributes['changeIndicator'])) {
            $identificationModel->spriden_change_ind = $identification->getChangeIndicator();
        };

        if (isset($modifiedAttributes['entityIndicator'])) {
            $identificationModel->spriden_entity_ind = $identification->getEntityIndicator();
        };

        $identificationModel->spriden_activity_date = $identification->getActivityDate()->format('Y-m-d H:i:s');
        $identificationModel->spriden_user_id = $identification->getUserId();
        $identificationModel->spriden_data_origin = $identification->getDataOrigin();

        $identificationModel->save();

        return true;
    }

    public function get(array $pidm)
    {
        $data = IdentificationModel::with('AdditionalIdentifications')->whereIn(
            'spriden_pidm',
            $pidm)->get();
        
        $identification = [];

        foreach ($data as $datum) {
            $identificationData = [];
            $identificationData['pidm'] = $datum['spriden_pidm'];
            $identificationData['muid'] = $datum['spriden_id'];
            $identificationData['lastName'] = $datum['spriden_last_name'];
            $identificationData['firstName'] = $datum['spriden_first_name'];
            $identificationData['middleName'] = $datum['spriden_mi'];
            $identificationData['changeIndicator'] = $datum['spriden_change_ind'];
            $identificationData['entityIndicator'] = $datum['spriden_entity_ind'];
            $identificationData['surrogateId'] = $datum['spriden_surrogate_id'];
            $identificationData['additonalIds'] = $this->createAdditionalIdentificationCollection($datum['AdditionalIdentifications']);
            $identification[] = $identificationData;
        }

        return $identification;
    }

    /** 
     *  @return AdditionalIdentificationCollection
     */
    private function createAdditionalIdentificationCollection(Collection $collection): AdditionalIdentificationCollection
    {
        $additionalIdentificationCollection = new AdditionalIdentificationCollection();
        foreach ($collection as $item) {
            $additionalIdentificationCollection->push($this->createAdditionalIdentification($item)->toJsonArray());
        }
        return $additionalIdentificationCollection;
    }

    /** 
     *  @return AdditionalIdentification
     */
    private function createAdditionalIdentification(Model $model): AdditionalIdentification
    {
        return new AdditionalIdentification(
            $model->goradid_adid_code,
            $model->goradid_additional_id
        );
    }
}
