<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 01/29/19
 * Time: 4:32 PM
 */

namespace MiamiOH\RestngPersonWebService\Repositories;

use MiamiOH\RestngPersonWebService\Objects\Identification;

interface IdentificationRepository
{
    public function create(Identification $identification): bool;

    public function update(Identification $identification): bool;

    public function get(array $pidm);
}
