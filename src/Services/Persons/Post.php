<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 1/29/19
 * Time: 6:03 PM
 */

namespace MiamiOH\RestngPersonWebService\Services\Persons;

use MiamiOH\RESTng\Util\User;
use MiamiOH\RestngPersonWebService\Objects\Person;
use MiamiOH\RestngPersonWebService\Repositories\PersonRepository;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;

/**
 * Class Post
 * @package MiamiOH\RestngPersonWebService\Services\Persons
 */
class Post
{
    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     * @param PersonRepository $repository
     * @return Response
     * @throws \Exception
     */
    public function postSingle(
        Request $request,
        Response $response,
        User $user,
        PersonRepository $repository
    ): Response
    {
        $status = \MiamiOH\RESTng\App::API_CREATED;

        $data = $request->getData();

        if (empty($data)) {
            $payload['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayload($payload);
            $response->setStatus($status);
            return $response;
        }

        $data['userId'] = $user->getUsername();
        $data['dataOrigin'] = 'WebService';

        try {
            $identification = Person::fromArray($data);
            $repository->create($identification);
        } catch (\Exception $e) {
            $response->setPayload([$e->getMessage()]);
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            return $response;
        }

        // DONE
        $response->setPayload(['1 record created.']);
        $response->setStatus($status);
        return $response;
    }

}