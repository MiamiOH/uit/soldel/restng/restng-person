<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 1/29/19
 * Time: 6:06 PM
 */

namespace MiamiOH\RestngPersonWebService\Services\Persons;

use MiamiOH\RESTng\Util\User;
use MiamiOH\RestngPersonWebService\Objects\Person;
use MiamiOH\RestngPersonWebService\Repositories\PersonRepository;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RestngPersonWebService\Repositories\PersonRepositorySQL;

/**
 * Class Put
 * @package MiamiOH\RestngPersonWebService\Services\Persons
 */
class Put
{
    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     * @param PersonRepository $repository
     * @return Response
     * @throws \Exception
     */
    public function putSingle(
        Request $request,
        Response $response,
        User $user,
        PersonRepository $repository
    ): Response
    {
        $status = \MiamiOH\RESTng\App::API_CREATED;

        $data = $request->getData();
        $pidm = $request->getResourceParam('pidm');

        if (empty($data)) {
            $payload['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayload($payload);
            $response->setStatus($status);
            return $response;
        }

        $data['userId'] = $user->getUsername();
        $data['dataOrigin'] = 'WebService';
        $data['pidm'] = $pidm;

        try {
            $person = Person::fromArray($data);

            $personSql = new PersonRepositorySQL();
            $personSql->update($person);
        } catch (\Exception $e) {
            $response->setPayload([$e->getMessage()]);
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            return $response;
        }

        // DONE
        $response->setPayload(['1 record updated']);
        $response->setStatus($status);
        return $response;
    }

}