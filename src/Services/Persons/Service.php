<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 1/29/19
 * Time: 5:58 PM
 */

namespace MiamiOH\RestngPersonWebService\Services\Persons;

use MiamiOH\RestngPersonWebService\Repositories\PersonRepository;
use MiamiOH\RestngPersonWebService\Repositories\PersonRepositorySQL;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RESTng\Legacy\DataSource;

class Service extends \MiamiOH\RESTng\Service
{
    /**
     * @var string
     */
    protected $dataSource = 'MUWS_SEC_PROD';

    /**
     * @var Request
     */
    private $request = null;

    /**
     * @var Response
     */
    private $response = null;

    /**
     * @var User
     */
    private $user = null;

    /**
     * @var PersonRepository
     */
    private $repository = null;


    public function getDependencies()
    {
        date_default_timezone_set('US/Eastern');

        $this->user = $this->getApiUser();
        $this->response = $this->getResponse();
        $this->request = $this->getRequest();

        $this->repository = new PersonRepositorySQL();
    }

    /**
     * @throws \Exception
     */
    public function postSingle()
    {
        $this->getDependencies();

        $postService = new Post();

        $response = $postService->postSingle(
            $this->request,
            $this->response,
            $this->user,
            $this->repository
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function putSingle()
    {
        $this->getDependencies();

        $putService = new Put();

        $response = $putService->putSingle(
            $this->request,
            $this->response,
            $this->user,
            $this->repository
        );

        return $response;
    }

}