<?php

namespace MiamiOH\RestngPersonWebService\Services;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Legacy\DB\DBH;
use MiamiOH\RESTng\Service\Extension\BannerUtil;
use MiamiOH\RESTng\Util\Response;

class Person extends \MiamiOH\RESTng\Service
{
    private $datasource = 'MUWS_SEC_PROD';
    /**
     * @var DBH
     */
    private $dbh;
    /**
     * @var BannerUtil
     */
    private $bannerUtil;

    private $emailDomain = '@miamioh.edu';

    /**
     * @return Response return a single of data (a Hash) - alternate keys.
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */

    public function getPersonsID()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $personID = $request->getResourceParam('personID');
        $keyField = 'pidm';

        switch ($request->getResourceParamKey('personID')) {
            case 'uniqueId':
                $keyField = 'uniqueId';
                break;
            case 'pidm':
                $keyField = 'pidm';
                break;
            case 'plus':
                $keyField = 'plus';
                break;
        }
        if ($personID === null) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);

            return $response;
        }

        if ($keyField === 'pidm') {
            $this->validateInput(
                $personID,
                $this->getPattern('pidm'),
                $this->getErrMesg('pidm')
            );
            $pidm = $personID;
        } elseif ($keyField === 'uniqueId') {
            $this->validateInput(
                $personID,
                $this->getPattern('uniqueId'),
                $this->getErrMesg('uniqueId')
            );
            $pidm = $this->getPidm(strtoupper($personID));
            if ($pidm == '-1000: EMPTY SET') {
                $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);

                return $response;
            }
        } elseif ($keyField === 'plus') {
            $this->validateInput(
                $personID,
                $this->getPattern('plus'),
                $this->getErrMesg('plus')
            );
            $pidm = $this->getPidmFromPlusNumber(strtoupper($personID));
        }

        $fieldList = $this->getFieldList();

        $query = "SELECT " . $fieldList . ",gzkagd1.fz_chk_privacy(spbpers_pidm) AS privacy_status
                FROM dual, spbpers
                LEFT JOIN szbuniq ON spbpers_pidm = szbuniq_pidm
                LEFT JOIN spriden ON spbpers_pidm = spriden_pidm
                LEFT JOIN goradid ON spbpers_pidm = goradid_pidm
                AND spriden_change_ind IS NULL
                LEFT JOIN gzbausr on gzbausr_pidm = spbpers_pidm
                AND goradid_adid_code='OHID'
                WHERE spbpers_pidm = ?";

        $payload = array();
        $payloadAnonymous = array();
        $preData = array();
        $payloadpre = array();
        $payloadpreAnonymous = array();
        $payloadpreViewAllWithSsn = array();
        $payloadViewAllWithSsn = array();
        $payloadPrivacy = array();
        $fieldListOriginal = $request->getPartialRequestFields();

        if ($request->isPartial()) {
            foreach ($fieldListOriginal as $fieldValue) {
                array_push($preData, $fieldValue);
            }
        }
        $stmt = $this->dbh->prepare($query);
        $stmt->execute($pidm);
        while ($data = $stmt->fetchrow_assoc()) {
            if ($request->isPartial()) {
                foreach ($preData as $dataLoad) {
                    switch ($dataLoad) {
                        case "plusNumber":
                            $payloadpre[$dataLoad] = $data['szbuniq_banner_id'];
                            break;
                        case "uniqueId":
                            $payloadpre[$dataLoad] = $data['szbuniq_unique_id'];
                            $payloadpreAnonymous[$dataLoad] = $data['szbuniq_unique_id'];
                            break;
                        case "pidm":
                            $payloadpre[$dataLoad] = $data['spriden_pidm'];
                            break;
                        case "ssn":
                            $payloadpreViewAllWithSsn[$dataLoad] = $data['spbpers_ssn'];
                            break;
                        case "prefix":
                            $payloadpre[$dataLoad] = $data['name_prefix'];
                            $payloadpreAnonymous[$dataLoad] = $data['name_prefix'];
                            break;
                        case "suffix":
                            $payloadpre[$dataLoad] = $data['name_suffix'];
                            $payloadpreAnonymous[$dataLoad] = $data['name_suffix'];
                            break;
                        case "givenNameLegal":
                            $payloadpre[$dataLoad] = $data['spriden_first_name'];
                            $payloadpreAnonymous[$dataLoad] = $data['spriden_first_name'];
                            break;
                        case "givenNamePreferred":
                            $namePreferred = empty($data['spbpers_pref_first_name']) ? $data['spriden_first_name'] : $data['spbpers_pref_first_name'];
                            $payloadpre[$dataLoad] = $namePreferred;
                            $payloadpreAnonymous[$dataLoad] = $namePreferred;
                            break;
                        case "familyName":
                            $payloadpre[$dataLoad] = $data['spriden_last_name'];
                            $payloadpreAnonymous[$dataLoad] = $data['spriden_last_name'];
                            break;
                        case "middleName":
                            $payloadpre[$dataLoad] = $data['name_middle'];
                            $payloadpreAnonymous[$dataLoad] = $data['name_middle'];
                            break;
                        case "age":
                            $payloadpre[$dataLoad] = date_diff(
                                date_create($data['birth_date']),
                                date_create('now')
                            )->y;
                            break;
                        case "birthDate":
                            $payloadpre[$dataLoad] = $data['birth_date'];
                            break;
                        case "emailAddress":
                            $payloadpre[$dataLoad] = ($data['szbuniq_unique_id'] !== ' ' ? strtolower($data['szbuniq_unique_id']) . $this->emailDomain : '');
                            break;
                        case "genderCode":
                            $payloadpre[$dataLoad] = $data['spbpers_sex'];
                            break;
                        case "genderDescription":
                            $payloadpre[$dataLoad] = $this->getGenderDescription($data['spbpers_sex']);
                            break;
                    }
                }
                $payload[] = $payloadpre;
                $payloadViewAllWithSsn[] = array_merge($payloadpre, $payloadpreViewAllWithSsn);
                $payloadAnonymous[] = $payloadpreAnonymous;
            } else {
                if (($data['privacy_status'] == 'N') || ($data['privacy_status'] == 'P')) {
                    $payload[] = array(
                        "plusNumber" => $data['szbuniq_banner_id'],
                        "uniqueId" => $data['szbuniq_unique_id'],
                        "pidm" => $data['spriden_pidm'],
                        "prefix" => $data['name_prefix'],
                        "suffix" => $data['name_suffix'],
                        "givenNameLegal" => $data['spriden_first_name'],
                        "givenNamePreferred" => empty($data['spbpers_pref_first_name']) ? $data['spriden_first_name'] : $data['spbpers_pref_first_name'],
                        "familyName" => $data['spriden_last_name'],
                        "middleName" => $data['name_middle'],
                        "age" => date_diff(
                            date_create($data['birth_date']),
                            date_create('now')
                        )->y,
                        "birthDate" => $data['birth_date'],
                        "emailAddress" => ($data['szbuniq_unique_id'] !== ' ' ? strtolower($data['szbuniq_unique_id']) . $this->emailDomain : ''),
                        "genderCode" => $data['spbpers_sex'],
                        "genderDescription" => $this->getGenderDescription($data['spbpers_sex'])
                    );

                    $payloadViewAllWithSsn = $payload;
                    $payloadViewAllWithSsn[0]['ssn'] = $data['spbpers_ssn'];

                    $payloadAnonymous[] = array(
                        "uniqueId" => $data['szbuniq_unique_id'],
                        "prefix" => $data['name_prefix'],
                        "suffix" => $data['name_suffix'],
                        "givenNameLegal" => $data['spriden_first_name'],
                        "givenNamePreferred" => empty($data['spbpers_pref_first_name']) ? $data['spriden_first_name'] : $data['spbpers_pref_first_name'],
                        "familyName" => $data['spriden_last_name'],
                        "middleName" => $data['name_middle']
                    );
                } else {
                    $payloadPrivacy[] = array(
                        "plusNumber" => $data['szbuniq_banner_id'],
                        "uniqueId" => $data['szbuniq_unique_id'],
                        "pidm" => $data['spriden_pidm'],
                        "prefix" => $data['name_prefix'],
                        "suffix" => $data['name_suffix'],
                        "givenNameLegal" => $data['spriden_first_name'],
                        "givenNamePreferred" => empty($data['spbpers_pref_first_name']) ? $data['spriden_first_name'] : $data['spbpers_pref_first_name'],
                        "familyName" => $data['spriden_last_name'],
                        "middleName" => $data['name_middle'],
                        "age" => date_diff(
                            date_create($data['birth_date']),
                            date_create('now')
                        )->y,
                        "birthDate" => $data['birth_date'],
                        "emailAddress" => ($data['szbuniq_unique_id'] !== ' ' ? strtolower($data['szbuniq_unique_id']) . $this->emailDomain : ''),
                        "genderCode" => $data['spbpers_sex'],
                        "genderDescription" => $this->getGenderDescription($data['spbpers_sex'])
                    );

                    $payloadViewAllWithSsn = $payloadPrivacy;
                    $payloadViewAllWithSsn[0]['ssn'] = $data['spbpers_ssn'];
                }
            }
        }

        $payloadAll = $payload;
        foreach ($payloadPrivacy as $valuepayloadPrivacy) {
            array_push($payloadAll, $valuepayloadPrivacy);
            array_push($payloadViewAllWithSsn, $valuepayloadPrivacy);
        }


        $user = $this->getApiUser();
        $isAuthenticated = $user->isAuthenticated();


        if (!$isAuthenticated) {
            if (empty($payloadAnonymous)) {
                $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            } else {
                $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                $response->setPayload($payloadAnonymous[0]); // Loading specific feilds
            }
        } // Token is passed through -> check Authorization of Token
        else {
            $isAuthorizedAll = $user->isAuthorized('WebServices', 'Person', 'All');
            $isAuthorizedView = $user->isAuthorized('WebServices', 'Person', 'view');
            $isAuthorizedViewAll = $user->isAuthorized(
                'WebServices',
                'Person',
                'viewAll'
            );
            $isAuthorizedViewAllWithSsn = $user->isAuthorized(
                'WebServices',
                'Person',
                'viewAllWithSsn'
            );

            $usernameToken = $user->getUserName();
            $pidmUsernameToken = $this->getPidm(strtoupper($usernameToken));
            if ($pidmUsernameToken == $pidm && !$isAuthorizedViewAllWithSsn) {
                $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                $response->setPayload($payloadAll[0]);
            } else {
                if ($isAuthorizedViewAllWithSsn) {
                    if (empty($payloadViewAllWithSsn[0])) {
                        $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
                    } else {
                        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                        $response->setPayload($payloadViewAllWithSsn[0]); // Loading specific feilds
                    }
                } elseif ($isAuthorizedViewAll || $isAuthorizedAll) {
                    if (empty($payloadAll[0])) {
                        $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
                    } else {
                        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                        $response->setPayload($payloadAll[0]); // Loading specific feilds
                    }
                } elseif ($isAuthorizedView) {
                    if (empty($payload[0])) {
                        $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
                    } else {
                        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                        $response->setPayload($payload[0]); // Loading specific feilds
                    }
                } else {
                    if (empty($payloadAnonymous[0])) {
                        $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
                    } else {
                        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                        $response->setPayload($payloadAnonymous[0]); // Loading specific feilds
                    }
                }
            }
        }

        return $response;
    }

    private function validateInput($input, $pattern, $errMesg)
    {
        if (is_array($input)) {
            foreach ($input as $value) {
                if (!preg_match($pattern, $value)) {
                    throw new \MiamiOH\RESTng\Exception\BadRequest($errMesg);
                }
            }
        } else {
            if (!preg_match($pattern, $input)) {
                throw new \MiamiOH\RESTng\Exception\BadRequest($errMesg);
            }
        }
    }

    private function getPattern($val)
    {
        if ($val == 'pidm') {
            return '/^\d{1,8}$/';
        } elseif ($val == 'uniqueId') {
            return '/^\w{1,8}$/';
        } elseif ($val == 'plus') {
            return '/^[+]\d{8}$/';
        } elseif ($val == 'effectiveTermCode') {
            return '/^\d{6}$/';
        }
    }

    private function getErrMesg($val)
    {
        if ($val == 'pidm') {
            return 'PIDM must be numeric and have less than or equal to 8 digits.';
        } elseif ($val == 'uniqueId') {
            return 'Unique ID must be all letter and have less than or equal to 8 digits.';
        } elseif ($val == 'plus') {
            return 'PlusNumber must be start with + and have exactly 8 digits ';
        } elseif ($val == 'effectiveTermCode') {
            return 'Invalid term code entered. Should be 6 digits only';
        }
    }

    protected function getPidm($uniqueId)
    {
        $pidm = $this->dbh->queryfirstcolumn(
            'select szbuniq_pidm from szbuniq where upper(szbuniq_unique_id) = upper(?)',
            $uniqueId
        );

        return $pidm;
    }

    protected function getPidmFromPlusNumber($plusNumber)
    {
        $pidm = $this->dbh->queryfirstcolumn(
            'select spriden_pidm from spriden where upper(spriden_id) = upper(?)',
            $plusNumber
        );

        return $pidm;
    }

    private function getFieldList()
    {
        $request = $this->getRequest();
        $fieldList = '';
        if ($request->isPartial()) {
            $fieldListOriginal = implode(
                ', ',
                $this->snakeCaseValues($request->getPartialRequestFields())
            );

            $replaceList = array(
                'unique_id' => "NVL(szbuniq_unique_id,' ') AS szbuniq_unique_id",
                'pidm' => "spriden_pidm",
                'ssn' => "spbpers_ssn",
                'plus_number' => "NVL(szbuniq_banner_id,' ') AS szbuniq_banner_id ",
                'everfi_id' => "NVL(goradid_additional_id,' ') AS goradid_additional_id ",
                'prefix' => 'NVL(spbpers_name_prefix,\' \') AS name_prefix',
                'suffix' => 'NVL(spbpers_name_suffix,\' \') AS name_suffix',
                'given_name_legal' => 'spriden_first_name',
                'given_name_preferred' => 'spbpers_pref_first_name',
                'family_name' => 'spriden_last_name',
                'middle_name' => 'NVL(spriden_mi,\' \') AS name_middle',
                'birth_date' => "to_char(spbpers_birth_date,'YYYY-MM-DD') AS birth_date",
                'gender_code' => 'spbpers_sex',
                'gender_description' => 'spbpers_sex',
                'age' => "to_char(spbpers_birth_date,'YYYY-MM-DD') AS birth_date"
            );

            $fieldList = str_replace(
                array_keys($replaceList),
                array_values($replaceList),
                $fieldListOriginal
            );
        } else {
            $fieldList = "
                NVL(szbuniq_banner_id,' ') AS szbuniq_banner_id,
                NVL(szbuniq_unique_id,' ') AS szbuniq_unique_id,
                spriden_pidm,
                spbpers_ssn,
                NVL(spbpers_name_prefix,' ') AS name_prefix,
                NVL(spriden_first_name,' ') AS spriden_first_name,
                spbpers_pref_first_name,
                NVL(spriden_mi,' ') AS name_middle,
                NVL(spriden_last_name,' ') AS spriden_last_name,
                NVL(spbpers_name_suffix,' ') AS name_suffix,
                NVL(to_char(spbpers_birth_date,'YYYY-MM-DD'),' ') AS birth_date,
                gzbausr_hide_middle_name,
                gzbausr_directory_listing,
                NVL(spbpers_sex,' ') AS spbpers_sex
            ";
        }

        return $fieldList;
    }

    protected function getGenderDescription($gender)
    {
        if ($gender == 'M') {
            return 'Male';
        }
        if ($gender == 'F') {
            return 'Female';
        }
        if (($gender == 'N') || ($gender == '')) {
            return ' ';
        }
    }

    /**
     * @return Response: return a collection of data (an Array)
     * @throws \Exception
     */
    public function getPersons()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $offset = $request->getOffset();
        $limit = $request->getLimit();
        $isPaged = $request->isPaged();

        $values = array();
        if (!$this->checkInput(
            $options['pidm'] ?? null,
            $options['uniqueId'] ?? null,
            $options['bannerPlusId'] ?? null,
            $options['familyName'] ?? null,
            $options['givenNameLegal'] ?? null
        )
        ) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);

            return $response;
        }

        //$uId=[];
        $pidms = [];
        $familyNameArray = [];
        $givenNameArray = [];
        $checkFamilyNameField = 0;
        $checkgivenNameLegalField = 0;
        $checkBoth = 0;
        $effectiveTermCode = '';
        $population = [];
        $join = '';

        if (isset($options['pidm'])) {
            $this->validateInput(
                $options['pidm'],
                $this->getPattern('pidm'),
                $this->getErrMesg('pidm')
            );
            $pidms = array_merge($pidms, $options['pidm']);
        }

        if (isset($options['uniqueId'])) {
            $this->validateInput(
                $options['uniqueId'],
                $this->getPattern('uniqueId'),
                $this->getErrMesg('uniqueId')
            );
            $pidms = array_merge($pidms, $this->getPidms($options['uniqueId']));
        }

        if (isset($options['bannerPlusId'])) {
            $this->validateInput(
                $options['bannerPlusId'],
                $this->getPattern('plus'),
                $this->getErrMesg('bannerPlusId')
            );
            $pidms = array_merge(
                $pidms,
                $this->getPidmsFromPlusNumbers($options['bannerPlusId'])
            );
        }

        if (!empty($options['effectiveTermCode'])) {
            $effectiveTermCode = $options['effectiveTermCode'];
            $this->validateInput(
                $effectiveTermCode,
                $this->getPattern('effectiveTermCode'),
                $this->getErrMesg('effectiveTermCode')
            );
        } else {
            // set to current term (maybe even sub-select fz_term_code?)
            $effectiveTermCode = "(select fz_get_term() as current_term from dual)";
        }

        if (isset($options['familyName'])) {
            $checkFamilyNameField = 1;
            $pidms = array_merge($pidms, $options['familyName']);
            $pidms = array_map('ucfirst', $pidms);
        }

        if (isset($options['givenNameLegal'])) {
            $checkgivenNameLegalField = 1;
            $pidms = array_merge($pidms, $options['givenNameLegal']);
            $pidms = array_map('ucfirst', $pidms);
        }

        if (isset($options['familyName']) && isset($options['givenNameLegal'])) {
            $checkBoth = 1;
            $familyNameArray = array_merge($familyNameArray, $options['familyName']);
            $familyNameArray = array_map('ucfirst', $familyNameArray);
            $givenNameArray = array_merge(
                $givenNameArray,
                $options['givenNameLegal']
            );
            $givenNameArray = array_map('ucfirst', $givenNameArray);
        }

        if (isset($options['population'])) {
            $population = $options['population'];
        }

        $pidmsReplace = '';
        $familyNameReplace = '';
        $givenNameReplace = '';
        $includeResidencyStatus = false;
        $where = '';

        if (empty($pidms) && empty($uniqueId) && empty($familyNameArray) && empty($givenNameArray) && empty($population)) {
            $where = "spbpers_pidm IN (null)";
        } else {
            $pidmsReplace = implode(',', array_fill(0, count($pidms), '?'));

            $familyNameReplace = implode(
                ',',
                array_fill(0, count($familyNameArray), '?')
            );

            $givenNameReplace = implode(
                ',',
                array_fill(0, count($givenNameArray), '?')
            );

            if (!empty($pidmsReplace)) {
                $where = "spbpers_pidm IN (" . $pidmsReplace . ")";
            }

            if (($checkFamilyNameField == 1) && ($checkgivenNameLegalField == 0)) {
                $where = "spriden_last_name IN (" . $pidmsReplace . ")";
            }

            if (($checkFamilyNameField == 0) && ($checkgivenNameLegalField == 1)) {
                $where = "spriden_first_name IN (" . $pidmsReplace . ")";
            }

            if ($checkBoth == 1) {
                $where = "spriden_first_name IN (" . $givenNameReplace . ") and spriden_last_name IN (" . $familyNameReplace . ")";
            }

            if ($population == 'currentActiveResidentStudents') {
                $includeResidencyStatus = true;
                if (!empty($where)) {
                    $where .= " AND ";
                }
                $where .= "student.student_term_code = $effectiveTermCode";
            }

            if (!empty($population)) {
                switch ($population) {
                    case "enrollmentEligibleStudents":
                        $join .= " INNER JOIN (with getterm as (select fz_get_term() as current_term from dual)select unique(sgbstdn_pidm) from sgbstdn where sgbstdn_term_code_eff >= (select current_term from getterm)) on spbpers_pidm = sgbstdn_pidm";
                        break;
                    case "currentActiveStudents":
                        $join .= " INNER JOIN (with getterm as (select fz_get_term() as current_term from dual)select unique(sgbstdn_pidm) from sgbstdn where sgbstdn_term_code_eff = (select current_term from getterm) and sgbstdn_stst_code = 'AS') on spbpers_pidm = sgbstdn_pidm";
                        break;
                    case "activeEligibleStudents":
                        $join .= " INNER JOIN (select unique(sgbstdn_pidm) from sgbstdn where sgbstdn_term_code_eff >= $effectiveTermCode and sgbstdn_stst_code in ('AS', 'NC')) on spbpers_pidm = sgbstdn_pidm";
                        break;
                    case "currentActiveResidentStudents":
                        $join .= " 
                            INNER JOIN (
                                SELECT DISTINCT 
                                    sgbstdn_pidm, 
                                    sgbstdn_term_code_eff AS student_term_code,
                                    sgbstdn_styp_code AS student_type_code,
                                    sgbstdn_resd_code AS student_resident_code
                                FROM sgbstdn
                                WHERE 
                                    sgbstdn_stst_code = 'AS'
                                    AND sgbstdn_user_id IS NOT NULL 
                                    AND sgbstdn_resd_code in ('R', 'Z') 
                            ) student ON student.sgbstdn_pidm = spbpers_pidm
                        ";
                        break;
                    case "activeEmployees":
                        $join .= " INNER JOIN (select unique(pebempl_pidm) from pebempl where pebempl_empl_status <> 'T') on spbpers_pidm = pebempl_pidm";
                        break;
                    case "emeriti":
                        $join .= " INNER JOIN (select unique(aprpros_pidm) from aprpros where aprpros_prcd_code IN ('SOME', 'SW', 'SSP')) on spbpers_pidm = aprpros_pidm";
                        break;
                    case "retirees":
                        $join .= " INNER JOIN (select unique(aprpros_pidm) from aprpros where aprpros_prcd_code IN ('SOMR','RSW','RSSP')) on spbpers_pidm = aprpros_pidm";
                        break;
                    case "inactiveEmployees":
                        $join .= " INNER JOIN (select unique(pebempl_pidm) from pebempl where pebempl_empl_status = 'T') on spbpers_pidm = pebempl_pidm";
                        break;
                    case "employeesWorkedRollingLastYear":
                        $join .= " INNER JOIN (select unique(pebempl_pidm) from pebempl  where pebempl_last_work_date > add_months(sysdate, -12) or pebempl_last_work_date is null) on spbpers_pidm = pebempl_pidm";
                        break;
                    case "alumni":
                        $join .= " INNER JOIN (select unique(aprcatg_pidm) from atvdonr, aprcatg where aprcatg_donr_code = atvdonr_code and atvdonr_alum_ind = 'Y') on spbpers_pidm = aprcatg_pidm";
                        break;
                    case "alumniRollingTwoYears":
                        $join .= " INNER JOIN (select unique(aprcatg_pidm) from atvdonr inner join aprcatg  on  aprcatg_donr_code = atvdonr_code left join shrdgmr on aprcatg_pidm = shrdgmr_pidm where atvdonr_alum_ind = 'Y' and SHRDGMR_grad_date > add_months(sysdate, -12*2)) on spbpers_pidm = aprcatg_pidm";
                        break;

                    default:
                        throw new \MiamiOH\RESTng\Exception\BadRequest("Invalid Population");

                }
            }
        }

        $fieldList = $this->getFieldList();

        /**
         * Only add these fields for this specific case, otherwise we may impact existing functionality
         */
        if ($includeResidencyStatus) {
            $fieldList .= ", student_resident_code, student_type_code, student_term_code ";
        }

        $query = "SELECT * FROM (
                    SELECT /*+ FIRST_ROWS(n) */ t.*, ROWNUM rnum 
                    FROM (
                        SELECT {$fieldList}, gzkagd1.fz_chk_privacy(spbpers_pidm) AS privacy_status 
                        FROM spbpers
                            LEFT JOIN szbuniq ON spbpers_pidm = szbuniq_pidm " . $join . "
                            LEFT JOIN spriden ON spbpers_pidm = spriden_pidm AND spriden_change_ind IS NULL
                            LEFT JOIN gzbausr ON gzbausr_pidm = spbpers_pidm
                        " . ((!empty($where)) ? 'WHERE ' . $where : '') . "
                        ORDER BY szbuniq_unique_id
                    ) t
                    WHERE ROWNUM <= " . ($offset + $limit - 1) . ")
                WHERE rnum >= " . ($offset);

        $stmt = $this->dbh->prepare($query);

        if ($checkBoth == 1) {
            $stmt->execute(array_merge($givenNameArray, $familyNameArray));
        } else {
            if (empty($pidmsReplace)) {
                $stmt->execute();
            } else {
                $stmt->execute($pidms);
            }
        }

        $payload = array();
        $payloadAnonymous = array();
        $preData = array();
        $payloadpre = array();
        $payloadpreViewAllWithSsn = array();
        $payloadViewAllWithSsn = array();
        $payloadPrivacy = array();
        $fieldListOriginal = $request->getPartialRequestFields();

        if ($request->isPartial()) {
            foreach ($fieldListOriginal as $fieldValue) {
                $preData[] = $fieldValue;
            }
        }

        $payloadpreAnonymous = array();

        /**
         * Note: Added $payloadPacket items because current implementation uses non-indexed arrays
         * So selectively adding data is not possible...this is a code smell!
         */
        $results_test = []; // for debugging only
        while ($data = $stmt->fetchrow_assoc()) {
            $results_test[] = $data;
            if ($request->isPartial()) {
                foreach ($preData as $dataLoad) {
                    switch ($dataLoad) {
                        case "plusNumber":
                            $payloadpre[$dataLoad] = $data['szbuniq_banner_id'];
                            break;
                        case "uniqueId":
                            $payloadpre[$dataLoad] = $data['szbuniq_unique_id'];
                            if (($data['privacy_status'] == 'N') || ($data['privacy_status'] == 'P')) {
                                $payloadpreAnonymous[$dataLoad] = $data['szbuniq_unique_id'];
                            }
                            break;
                        case "ssn":
                            $payloadpreViewAllWithSsn[$dataLoad] = $data['spbpers_ssn'];
                            break;
                        case "pidm":
                            $payloadpre[$dataLoad] = $data['spriden_pidm'];
                            break;
                        case "prefix":
                            $payloadpre[$dataLoad] = $data['name_prefix'];
                            if (($data['privacy_status'] == 'N') || ($data['privacy_status'] == 'P')) {
                                $payloadpreAnonymous[$dataLoad] = $data['name_prefix'];
                            }
                            break;
                        case "suffix":
                            $payloadpre[$dataLoad] = $data['name_suffix'];
                            if (($data['privacy_status'] == 'N') || ($data['privacy_status'] == 'P')) {
                                $payloadpreAnonymous[$dataLoad] = $data['name_suffix'];
                            }
                            break;
                        case "givenNameLegal":
                            $payloadpre[$dataLoad] = $data['spriden_first_name'];
                            if (($data['privacy_status'] == 'N') || ($data['privacy_status'] == 'P')) {
                                $payloadpreAnonymous[$dataLoad] = $data['spriden_first_name'];
                            }
                            break;
                        case "givenNamePreferred":
                            $namePreferred = empty($data['spbpers_pref_first_name']) ? $data['spriden_first_name'] : $data['spbpers_pref_first_name'];
                            $payloadpre[$dataLoad] = $namePreferred;
                            if (($data['privacy_status'] == 'N') || ($data['privacy_status'] == 'P')) {
                                $payloadpreAnonymous[$dataLoad] = $namePreferred;
                            }
                            break;
                        case "familyName":
                            $payloadpre[$dataLoad] = $data['spriden_last_name'];
                            $payloadpreAnonymous[$dataLoad] = $data['spriden_last_name'];
                            break;
                        case "middleName":
                            $payloadpre[$dataLoad] = $data['name_middle'];
                            if (($data['privacy_status'] == 'N') || ($data['privacy_status'] == 'P')) {
                                $payloadpreAnonymous[$dataLoad] = $data['name_middle'];
                            }
                            break;
                        case "age":
                            $payloadpre[$dataLoad] = date_diff(
                                date_create($data['birth_date']),
                                date_create('now')
                            )->y;
                            break;
                        case "birthDate":
                            $payloadpre[$dataLoad] = $data['birth_date'];
                            break;
                        case "emailAddress":
                            $payloadpre[$dataLoad] = ($data['szbuniq_unique_id'] !== ' ' ? strtolower($data['szbuniq_unique_id']) . $this->emailDomain : '');
                            break;
                        case "genderCode":
                            $payloadpre[$dataLoad] = $data['spbpers_sex'];
                            break;
                        case "genderDescription":
                            $payloadpre[$dataLoad] = $this->getGenderDescription($data['spbpers_sex']);
                            break;
                        case "residencyStatus":
                            if ($population == 'currentActiveResidentStudents') {
                                $payloadpre[$dataLoad] = $data['residencyStatus'];
                            }
                            break;
                    }
                }
                $payload[] = $payloadpre;
                $payloadViewAllWithSsn[] = array_merge($payloadpre, $payloadpreViewAllWithSsn);
                $payloadAnonymous[] = $payloadpreAnonymous;
            } else {
                $firstName = trim($data['spriden_first_name']);
                if ($firstName === null || $firstName === '') {
                    $firstName = null;
                }

                $lastName = trim($data['spriden_last_name']);
                if ($lastName === null || $lastName === '') {
                    $lastName = null;
                }

                $middleName = trim($data['name_middle']);
                if ($middleName === null || $middleName === '') {
                    $middleName = null;
                }

                $suffix = trim($data['name_suffix']);
                if ($suffix === null || $suffix === '') {
                    $suffix = null;
                }

                $prefix = trim($data['name_prefix']);
                if ($prefix === null || $prefix === '') {
                    $prefix = null;
                }

                if (($data['privacy_status'] == 'N') || ($data['privacy_status'] == 'P')) {
                    $payloadPacket = array(
                        "plusNumber" => $data['szbuniq_banner_id'],
                        "uniqueId" => $data['szbuniq_unique_id'],
                        "pidm" => $data['spriden_pidm'],
                        "prefix" => $data['name_prefix'],
                        "suffix" => $data['name_suffix'],
                        "givenNameLegal" => $data['spriden_first_name'],
                        "givenNamePreferred" => empty($data['spbpers_pref_first_name']) ? $data['spriden_first_name'] : $data['spbpers_pref_first_name'],
                        "familyName" => $data['spriden_last_name'],
                        "middleName" => $data['name_middle'],
                        "age" => date_diff(
                            date_create($data['birth_date']),
                            date_create('now')
                        )->y,
                        "birthDate" => $data['birth_date'],
                        "emailAddress" => ($data['szbuniq_unique_id'] !== ' ' ? strtolower($data['szbuniq_unique_id']) . $this->emailDomain : ''),
                        "genderCode" => $data['spbpers_sex'],
                        "genderDescription" => $this->getGenderDescription($data['spbpers_sex']),
                        'hideMiddleName' => $data['gzbausr_hide_middle_name'] === 'Y',
                        'directoryListing' => $data['gzbausr_directory_listing'] ?? 'L',
                        'informalDisplayedName' => $this->getInformalDisplayedName(
                            $firstName,
                            $lastName,
                            $middleName,
                            $suffix
                        ),
                        'formalDisplayedName' => $this->getFormalDisplayedName(
                            $firstName,
                            $lastName,
                            $middleName,
                            $suffix,
                            $prefix
                        ),
                        'informalSortedName' => $this->getInformalSortedName(
                            $firstName,
                            $lastName,
                            $middleName,
                            $suffix
                        ),
                        'formalSortedName' => $this->getFormalSortedName(
                            $firstName,
                            $lastName,
                            $middleName,
                            $suffix,
                            $prefix
                        ),
                        'ssn' => $data['spbpers_ssn']
                    );

                    if ($includeResidencyStatus) {
                        $payloadPacket['residencyStatus'] = $data['student_resident_code'];
                        $payloadPacket['studentTypeCode'] = $data['student_type_code'];
                    }

                    $payload[] = $payloadPacket;

                    $payloadViewAllWithSsn = $payload;
                    //$payloadViewAllWithSsn[0]['ssn'] = $data['spbpers_ssn'];

                    $payloadPacket = array(
                        "uniqueId" => $data['szbuniq_unique_id'],
                        "prefix" => $data['name_prefix'],
                        "suffix" => $data['name_suffix'],
                        "givenNameLegal" => $data['spriden_first_name'],
                        "givenNamePreferred" => empty($data['spbpers_pref_first_name']) ? $data['spriden_first_name'] : $data['spbpers_pref_first_name'],
                        "familyName" => $data['spriden_last_name'],
                        "middleName" => $data['name_middle'],
                        'hideMiddleName' => $data['gzbausr_hide_middle_name'] === 'Y',
                        'directoryListing' => $data['gzbausr_directory_listing'] ?? 'L',
                        'informalDisplayedName' => $this->getInformalDisplayedName(
                            $firstName,
                            $lastName,
                            $middleName,
                            $suffix
                        ),
                        'formalDisplayedName' => $this->getFormalDisplayedName(
                            $firstName,
                            $lastName,
                            $middleName,
                            $suffix,
                            $prefix
                        ),
                        'informalSortedName' => $this->getInformalSortedName(
                            $firstName,
                            $lastName,
                            $middleName,
                            $suffix
                        ),
                        'formalSortedName' => $this->getFormalSortedName(
                            $firstName,
                            $lastName,
                            $middleName,
                            $suffix,
                            $prefix
                        )
                    );

                    if ($includeResidencyStatus) {
                        $payloadPacket['residencyStatus'] = $data['student_resident_code'];
                        $payloadPacket['studentTypeCode'] = $data['student_type_code'];
                    }

                    $payloadAnonymous[] = $payloadPacket;

                } else {
                    $payloadPacket = array(
                        "plusNumber" => $data['szbuniq_banner_id'],
                        "uniqueId" => $data['szbuniq_unique_id'],
                        "pidm" => $data['spriden_pidm'],
                        "prefix" => $data['name_prefix'],
                        "suffix" => $data['name_suffix'],
                        "givenNameLegal" => $data['spriden_first_name'],
                        "givenNamePreferred" => empty($data['spbpers_pref_first_name']) ? $data['spriden_first_name'] : $data['spbpers_pref_first_name'],
                        "familyName" => $data['spriden_last_name'],
                        "middleName" => $data['name_middle'],
                        "age" => date_diff(
                            date_create($data['birth_date']),
                            date_create('now')
                        )->y,
                        "birthDate" => $data['birth_date'],
                        "emailAddress" => ($data['szbuniq_unique_id'] !== ' ' ? strtolower($data['szbuniq_unique_id']) . $this->emailDomain : ''),
                        'hideMiddleName' => $data['gzbausr_hide_middle_name'] === 'Y' ? true : false,
                        'directoryListing' => $data['gzbausr_directory_listing'] ?? 'L',
                        "genderCode" => $data['spbpers_sex'],
                        "genderDescription" => $this->getGenderDescription($data['spbpers_sex']),
                        'informalDisplayedName' => $this->getInformalDisplayedName(
                            $firstName,
                            $lastName,
                            $middleName,
                            $suffix
                        ),
                        'formalDisplayedName' => $this->getFormalDisplayedName(
                            $firstName,
                            $lastName,
                            $middleName,
                            $suffix,
                            $prefix
                        ),
                        'informalSortedName' => $this->getInformalSortedName(
                            $firstName,
                            $lastName,
                            $middleName,
                            $suffix
                        ),
                        'formalSortedName' => $this->getFormalSortedName(
                            $firstName,
                            $lastName,
                            $middleName,
                            $suffix,
                            $prefix
                        ),
                        'ssn' => $data['spbpers_ssn']
                    );

                    if ($includeResidencyStatus) {
                        $payloadPacket['residencyStatus'] = $data['student_resident_code'];
                        $payloadPacket['studentTypeCode'] = $data['student_type_code'];
                    }

                    $payloadPrivacy[] = $payloadPacket;
                    $payloadViewAllWithSsn = $payloadPrivacy;
                    //$payloadViewAllWithSsn[0]['ssn'] = $data['spbpers_ssn'];
                }
            }
        }

        $payloadAll = $payload;
        foreach ($payloadPrivacy as $valuepayloadPrivacy) {
            array_push($payloadAll, $valuepayloadPrivacy);
            array_push($payloadViewAllWithSsn, $valuepayloadPrivacy);
        }

        $user = $this->getApiUser();
        $isAuthenticated = $user->isAuthenticated();

        //Anonymous Mode : Token is not passed.
        $finalPayload = [];
        if (!$isAuthenticated) {
            if (empty($payloadAnonymous)) {
                $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            } else {
                $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                $finalPayload = $payloadAnonymous; // Loading specific fields
            }
        } // Token is passed through -> check Authorization of Token
        else {
            $isAuthorizedAll = $user->isAuthorized('WebServices', 'Person', 'All');
            $isAuthorizedView = $user->isAuthorized('WebServices', 'Person', 'view');
            $isAuthorizedViewAll = $user->isAuthorized(
                'WebServices',
                'Person',
                'viewAll'
            );
            $isAuthorizedViewAllWithSsn = $user->isAuthorized(
                'WebServices',
                'Person',
                'viewAllWithSsn'
            );

            if ($isAuthorizedViewAllWithSsn) {
                if (empty($payloadViewAllWithSsn)) {
                    $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                } else {
                    $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                    $finalPayload = $payloadViewAllWithSsn;
                }
            } elseif ($isAuthorizedViewAll || $isAuthorizedAll) {
                if (empty($payloadAll)) {
                    $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                } else {
                    $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                    $finalPayload = $payloadAll;
                }
            } elseif ($isAuthorizedView) {
                if (empty($payload)) {
                    $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                } else {
                    $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                    $finalPayload = $payload;
                }
            } else {
                if (empty($payloadAnonymous)) {
                    $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                } else {
                    $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                    $finalPayload = $payloadAnonymous;
                }
            }
        }

        $response->setPayload($finalPayload);

        if (!empty($where)) {
            $where = 'WHERE ' . $where;
        }

        $query = "SELECT count(unique(spbpers_pidm)) as total" . "
               FROM dual,spbpers
               LEFT JOIN szbuniq ON spbpers_pidm = szbuniq_pidm " . $join . "
               LEFT JOIN spriden ON spbpers_pidm = spriden_pidm
               AND spriden_change_ind IS NULL
                " . $where;

        $stmt = $this->dbh->prepare($query);
        if ($checkBoth == 1) {
            $stmt->execute(array_merge($givenNameArray, $familyNameArray));
        } else {
            if (empty($pidmsReplace)) {
                $stmt->execute();
            } else {
                $stmt->execute($pidms);
            }
        }
        $results = $stmt->fetchrow_assoc();

        $totalObjects = 0;

        if (isset($results['total'])) {
            $totalObjects = $results['total'];
        }

        $response->setTotalObjects($totalObjects);

        return $response;
    }

    private function checkInput($pidm, $uniqueId, $familyName, $givenName)
    {
        if (($pidm && $uniqueId)
            || ($pidm && $familyName)
            || ($pidm && $givenName)
            || ($uniqueId && $familyName)
            || ($uniqueId && $givenName)
            || ($pidm && $uniqueId && $familyName)
            || ($uniqueId && $uniqueId && $familyName)
        ) {
            return 0;
        } else {
            return 1;
        }
    }

    protected function getPidms($uniqueIds)
    {
        if (!$this->dbh) {
            $this->dbh = $this->database->getHandle($this->datasource_name);
        }
        //$uniqueIds=array($uniqueIds);
        $uniqueIds = array_map(function ($val) {
            return strtoupper(trim($val));
        }, $uniqueIds);

        $records = $this->dbh->queryall_array("SELECT szbuniq_pidm FROM szbuniq WHERE szbuniq_unique_id IN (?" . str_repeat(
                ",?",
                count($uniqueIds) - 1
            ) . ")", $uniqueIds);
        $pidms = array_column($records, 'szbuniq_pidm');

        //if (empty($pidms)) return ' ';
        return $pidms;
    }

    protected function getPidmsFromPlusNumbers($plusNumbers)
    {
        $records = $this->dbh->queryall_array(
            'select spriden_pidm from spriden where upper(spriden_id) in (?' . str_repeat(
                ",?",
                count($plusNumbers) - 1
            ) . ')',
            $plusNumbers
        );
        $pidms = array_column($records, 'spriden_pidm');

        return $pidms;
    }

    private function getInformalDisplayedName(
        ?string $firstName,
        string  $lastName,
        ?string $middleName,
        ?string $suffix
    ): string
    {
        return sprintf(
            "%s%s %s%s",
            $firstName ?? '',
            ($middleName !== null ? ' ' . $middleName : ''),
            $lastName,
            $suffix !== null ? ', ' . $suffix : ''
        );
    }

    private function getFormalDisplayedName(
        ?string $firstName,
        string  $lastName,
        ?string $middleName,
        ?string $suffix,
        ?string $prefix
    ): string
    {
        return sprintf(
            "%s %s%s %s%s",
            $prefix,
            $firstName ?? '',
            $middleName !== null ? ' ' . $middleName : '',
            $lastName,
            $suffix !== null ? ', ' . $suffix : ''
        );
    }

    private function getInformalSortedName(
        ?string $firstName,
        string  $lastName,
        ?string $middleName,
        ?string $suffix
    ): string
    {
        return sprintf(
            "%s, %s %s%s",
            $lastName,
            $firstName ?? '',
            $middleName ?? '',
            $suffix !== null ? ', ' . $suffix : ''
        );
    }

    private function getFormalSortedName(
        ?string $firstName,
        string  $lastName,
        ?string $middleName,
        ?string $suffix,
        ?string $prefix
    ): string
    {
        return sprintf(
            "%s, %s%s %s%s",
            $lastName,
            $firstName ?? '',
            $middleName !== null ? ' ' . $middleName : '',
            $prefix,
            $suffix !== null ? ', ' . $suffix : ''
        );
    }

    public function getByMajors()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $limit = $request->getLimit();
        $offset = $request->getOffset();

        $majorCodes = $request->getOptions()['majorCodes'] ?? [];

        $majorCodes = array_map(function (string $str) {
            return strtoupper($str);
        }, $majorCodes);

        if (count($majorCodes) === 0) {
            return $response;
        }

        $questionMarks = implode(',', array_fill(0, count($majorCodes), '?'));

        $query = "
        SELECT * FROM
        (
          SELECT a.*, rownum r__
          FROM
          (
            select m.sgbstdn_pidm 
              from (
                select distinct sgbstdn_pidm 
                from sgbstdn 
                where sgbstdn_majr_code_1 in (" . $questionMarks . ") 
                  or sgbstdn_majr_code_2 in (" . $questionMarks . ")
              ) m
            WHERE BANINST1.fz_enrolled_this_term(m.sgbstdn_pidm, BANINST1.fz_get_term('N','S')) = 'Y'
            order by m.sgbstdn_pidm
          ) a
          WHERE rownum < ?
        )
        WHERE r__ >= ?
        ";

        $results = $this->dbh->queryall_array(
            $query,
            array_merge(
                $majorCodes,
                $majorCodes,
                [$limit + $offset, $offset]
            )
        );

        $pidms = array_map(function (array $row) {
            return $row['sgbstdn_pidm'];
        }, $results);

        $results = $this->callResource(
            'person.read.collection.v3',
            [
                'options' => [
                    'pidm' => $pidms
                ]
            ]
        );

        return $results;
    }

    public function getPersonIdPhoto()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        try {
            $bannerId = $this->bannerUtil->getId(
                $request->getResourceParamKey('muid'),
                $request->getResourceParam('muid')
            );
        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);

            return $response;
        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdTooManyMatches $e) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);

            return $response;
        }

        $pidm = $bannerId->getPidm();

        if (!$this->dbh->queryFirstColumn('select count(*) from gzbpict where gzbpict_pidm = ' . $pidm)) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);

            return $response;
        }

        $query = 'select gzbpict_pict from gzbpict where gzbpict_pidm = ?';

        $value = $this->dbh->queryFirstColumn($query, $pidm)->load();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload([
            'pidm' => $pidm,
            'uniqueId' => $bannerId->getUniqueId(),
            'jpeg' => base64_encode($value)
        ]);

        return $response;
    }

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource);
    }

    public function setBannerUtil($bannerUtil)
    {
        /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
        $this->bannerUtil = $bannerUtil;
    }

    protected function checkExistUniqueID($uniqueId)
    {
        $status = '';
        $status = $this->dbh->queryfirstcolumn(
            'select szbuniq_unique_id from szbuniq where upper(szbuniq_unique_id) = upper(?)',
            $uniqueId
        );

        return ($status == '-1000: EMPTY SET') ? false : true;
    }

    protected function checkExistPidm($pidm)
    {
        $status = '';
        $status = $this->dbh->queryfirstcolumn(
            'select szbuniq_pidm from szbuniq where szbuniq_pidm = ?',
            $pidm
        );

        return ($status == '-1000: EMPTY SET') ? false : true;
    }

    protected function getUniqueID($pidm)
    {
        $unique = $this->dbh->queryfirstcolumn(
            'select szbuniq_unique_id from szbuniq where szbuniq_pidm = ?',
            $pidm
        );

        return $unique;
    }

    protected function getPrivacyStatus($pidm)
    {
        $privacyStatus = $this->dbh->queryfirstcolumn('
                                                  SELECT gzkagd1.fz_chk_privacy(?)
                                                  FROM dual', $pidm);

        return $privacyStatus;
    }

}
