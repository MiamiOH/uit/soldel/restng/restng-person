<?php
/**
 * Author: liaom
 * Date: 7/31/18
 * Time: 10:10 AM
 */

namespace MiamiOH\RestngPersonWebService\Services;


use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Legacy\DB\DBH;
use MiamiOH\RESTng\Service;

class UpdatePersonService extends Service
{
    /**
     * @var DBH
     */
    private $dbh;

    public function setDatabaseFactory(DatabaseFactory $databaseFactory)
    {
        $this->dbh = $databaseFactory->getHandle('MUWS_SEC_PROD');
    }

    public function update()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $uniqueId = $this->getOption('uniqueId');
        $preferredFirstName = $this->getOption('preferredFirstName');
        $directoryListing = $this->getOption('directoryListing');
        $hideMiddleName = $this->getOption('hideMiddleName');

        if ($preferredFirstName !== null && strlen($preferredFirstName) > 60) {
            $response->setPayload([
                'message' => 'preferredFirstName cannot exceed 60 bytes'
            ]);
            $response->setStatus(App::API_BADREQUEST);

            return $response;
        }

        if ($directoryListing !== null && !in_array($directoryListing, ['L', 'U'])) {
            $response->setPayload([
                'message' => 'invalid directoryListing, only "L" and "U" are allowed'
            ]);
            $response->setStatus(App::API_BADREQUEST);

            return $response;
        }

        if ($hideMiddleName !== null && !in_array($hideMiddleName,
                ['true', 'false'])) {
            $response->setPayload([
                'message' => 'invalid hideMiddleName, only "true" and "false" are allowed'
            ]);
            $response->setStatus(App::API_BADREQUEST);

            return $response;
        }

        $getPidmByUniqueIdQuery = 'select szbuniq_pidm from szbuniq where szbuniq_unique_id = ?';
        $results = $this->dbh->queryall_array($getPidmByUniqueIdQuery,
            strtoupper($uniqueId));
        if (count($results) === 0) {
            $response->setPayload([
                'message' => sprintf('person not found by uniqueid "%s"', $uniqueId)
            ]);
            $response->setStatus(App::API_BADREQUEST);

            return $response;
        }
        $pidm = $results[0]['szbuniq_pidm'];

        if ($preferredFirstName !== null) {
            $this->updatePreferredName($pidm, $preferredFirstName);
        }

        if ($directoryListing !== null) {
            $this->updateDirectoryListing($pidm, $directoryListing);
        }

        if ($hideMiddleName !== null) {
            $hideMiddleName = $hideMiddleName === 'true' ? true : false;
            $this->updateHideMiddleName($pidm, $hideMiddleName);
        }

        return $response;
    }


    private function getOption(string $key): ?string
    {
        $options = $this->getRequest()->getOptions();
        if (!isset($options[$key])) {
            return null;
        }

        return $options[$key];
    }

    private function updatePreferredName(string $pidm, string $name): void
    {
        if ($name === '') {
            $name = null;
        }
        $query = 'update spbpers set spbpers_pref_first_name = ? where spbpers_pidm = ?';

        $this->dbh->perform($query, [$name, $pidm]);
    }

    private function updateDirectoryListing(
        string $pidm,
        string $directoryListing
    ): void {
        $query = 'update gzbausr set gzbausr_directory_listing = ? where gzbausr_pidm = ?';
        $this->dbh->perform($query, [$directoryListing, $pidm]);
        $this->forceRTAGUpdate($pidm, ['OPENLDAP', 'ADIT', 'GOOGLE', 'LRAY']);
    }

    private function updateHideMiddleName(string $pidm, bool $hideMiddleName): void
    {
        $query = 'update gzbausr set gzbausr_hide_middle_name = ? where gzbausr_pidm = ?';
        $this->dbh->perform($query, [$hideMiddleName ? 'Y' : 'N', $pidm]);
        $this->forceRTAGUpdate($pidm, ['OPENLDAP', 'ADIT', 'GOOGLE', 'LRAY']);
    }

    private function forceRTAGUpdate(string $pidm, array $services)
    {
        $questionMarks = implode(',', array_fill(0, count($services), '?'));

        $services = array_map(function (string $service) {
            return strtoupper($service);
        }, $services);

        $query = "
        update gzragst 
        set gzragst_status_ind = 'M',
          gzragst_progress_ind = 'P'
        where gzragst_pidm = ?
        and gzragst_server_name in ({$questionMarks})
        ";

        array_unshift($services, $pidm);

        $this->dbh->perform($query, $services);
    }
}