<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 2/8/19
 * Time: 10:59 AM
 */

namespace MiamiOH\RestngPersonWebService\Services\Identification;

use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RestngPersonWebService\EloquentModels\IdentificationModel;
use MiamiOH\RestngPersonWebService\Repositories\IdentificationRepositorySQL;

class Get
{
    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    public function getIdentification(
        Request $request,
        Response $response
    )
    {
        $status = \MiamiOH\RESTng\App::API_OK;

        $options = $request->getOptions();

        $identificationSql = new IdentificationRepositorySQL(new IdentificationModel());

        if (empty($options['pidm'])){
            throw new \MiamiOH\RESTng\Exception\BadRequest('Please provide correct pidm');
        }

        $identificationDetails = $identificationSql->get($options['pidm']);

        if (empty($identificationDetails)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('No record found.');
        }

        $response->setStatus($status);
        $response->setPayload($identificationDetails);

        return $response;

    }

}