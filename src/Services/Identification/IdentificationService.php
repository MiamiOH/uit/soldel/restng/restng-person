<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 1/29/19
 * Time: 5:58 PM
 */

namespace MiamiOH\RestngPersonWebService\Services\Identification;

use MiamiOH\RestngPersonWebService\Repositories\IdentificationRepository;
use MiamiOH\RestngPersonWebService\Repositories\IdentificationRepositorySQL;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RESTng\Legacy\DataSource;
use MiamiOH\RestngPersonWebService\Repositories\AdditionalIdentificationRepository;
use MiamiOH\RestngPersonWebService\Repositories\AdditionalIdentificationRepositorySQL;

class IdentificationService extends \MiamiOH\RESTng\Service
{
    /**
     * @var string
     */
    protected $dataSource = 'MUWS_SEC_PROD';

    /**
     * @var Request
     */
    private $request = null;

    /**
     * @var Response
     */
    private $response = null;

    /**
     * @var User
     */
    private $user = null;

    /**
     * @var IdentificationRepository
     */
    private $repository = null;

    /**
     * @var AdditionalIdentificationRepository
     */
    private $additionalIdentificationRepository = null;



    public function getDependencies()
    {
        date_default_timezone_set('US/Eastern');

        $this->user = $this->getApiUser();
        $this->response = $this->getResponse();
        $this->request = $this->getRequest();

        $this->repository = new IdentificationRepositorySQL();
        $this->additionalIdentificationRepository = new AdditionalIdentificationRepositorySQL();
    }

    /**
     * @throws \Exception
     */
    public function postSingle()
    {
        $this->getDependencies();

        $postService = new Post();

        $response = $postService->postSingle(
            $this->request,
            $this->response,
            $this->user,
            $this->repository
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function putSingle()
    {
        $this->getDependencies();

        $putService = new Put();

        $response = $putService->putSingle(
            $this->request,
            $this->response,
            $this->user,
            $this->repository
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function getIdentification()
    {
        $this->getDependencies();

        $getService = new Get();

        $response = $getService->getIdentification(
            $this->request,
            $this->response
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function postSingleAdditionalIdentification()
    {
        $this->getDependencies();

        $postService = new PostAdditionalIdentification();

        $response = $postService->postSingleAdditionalIdentification(
            $this->request,
            $this->response,
            $this->user,
            $this->additionalIdentificationRepository
        );

        return $response;
    }


}