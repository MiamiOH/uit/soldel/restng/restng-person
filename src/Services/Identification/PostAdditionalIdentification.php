<?php


namespace MiamiOH\RestngPersonWebService\Services\Identification;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RestngPersonWebService\Repositories\AdditionalIdentificationRepository;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RestngPersonWebService\Requests\AdditionalIdentificationRequest;

/**
 * Class PostAdditionalIdentification
 * @package MiamiOH\RestngPersonWebService\Services\Identification
 */
class PostAdditionalIdentification
{
    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     * @param AdditionalIdentificationRepository $repository
     * @return Response
     * @throws \Exception
     */
    public function postSingleAdditionalIdentification(
        Request $request,
        Response $response,
        User $user,
        AdditionalIdentificationRepository $repository
    ): Response
    {
        $status = \MiamiOH\RESTng\App::API_CREATED;

        $data = $request->getData();

        if (empty($data)) {
            $payload['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayload($payload);
            $response->setStatus($status);
            return $response;
        }

        
        $data['userId'] = $user->getUsername();
        $data['dataOrigin'] = 'WebService';

        try {
            $additionalIdentificationRequest = new AdditionalIdentificationRequest(
                $data['pidm'],
                $data['additionalIdCode'],
                $data['additionalId'],
                $data['userId'],
                $data['dataOrigin'],
                Carbon::now()->format('Y-m-d H:i:s')
            );
            $repository->create($additionalIdentificationRequest);
        } catch (ModelNotFoundException $e) {
            $response->setPayload([$e->getMessage()]);
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        } catch (\Exception $e) {
            $response->setPayload([$e->getMessage()]);
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            return $response;
        }

        // DONE
        $response->setPayload(['1 record created.']);
        $response->setStatus($status);
        return $response;
    }

}