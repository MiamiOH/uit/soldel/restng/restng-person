<?php

namespace MiamiOH\RestngPersonWebService\Requests;

class AdditionalIdentificationRequest
{
    /**
     * 
     * @var string
     */
    private $pidm;
    /**
     * 
     * @var string
     */
    private $additionalIdCode;
    /**
     *
     * @var string
     */
    private $userId;
    /**
     *
     * @var string
     */
    private $dataOrigin;
    /**
     *
     * @var string
     */
    private $activityDate;
    /**
     * @param string $$pidm
     * @param string $additionalIdCode
     * @param string $additionalId
     * @param string $userId
     * @param string $dataOrigin
     * @param string $activityDate
     * @return void
     */
    public function __construct(
        string $pidm, 
        string $additionalIdCode, 
        string $additionalId,
        string $userId,
        string $dataOrigin,
        string $activityDate)
    {
        $this->pidm = $pidm;
        $this->additionalIdCode = $additionalIdCode;
        $this->additionalId = $additionalId;
        $this->userId = $userId;
        $this->dataOrigin = $dataOrigin;
        $this->activityDate = $activityDate;
    }

    /**
     * return pidm
     *
     * @return string
     */
    public function getPidm(): string
    {
        return $this->pidm;
    }

    /**
     * return additional identification code
     *
     * @return string
     */
    public function getAdditionalIdCode(): string
    {
        return $this->additionalIdCode;
    }

    /**
     * return additional id
     *
     * @return string
     */
    public function getAdditionalId(): string
    {
        return $this->additionalId;
    }

    /**
     * return user id
     *
     * @return string
     */
    public function getuserId(): string
    {
        return $this->userId;
    }

     /**
     * return additional identification
     *
     * @return string
     */
    public function getDataOrigin(): string
    {
        return $this->dataOrigin;
    }

    /**
     * return activity date
     *
     * @return string
     */
    public function getActivityDate(): string
    {
        return $this->activityDate;
    }

    /**
     * 
     * @return array
     */
    public function toJsonArray(): array
    {
        return [
            'pidm' => $this->getPidm(),
            'additionalIdCode' => $this->getAdditionalIdCode(),
            'additionalId' => $this->getAdditionalId(),
            'userId' => $this->getuserId(),
            'dataOrigin' => $this->getDataOrigin(),
            'activityDate' => $this->getActivityDate()
        ];
    }
    
}
