<?php

namespace MiamiOH\RestngPersonWebService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;
use MiamiOH\RestngPersonWebService\Services\Persons\Service;
use MiamiOH\RestngPersonWebService\Services\Identification\IdentificationService;

class PersonResourceProvider extends ResourceProvider
{

    private $tag = 'Person';
    private $name = 'persons';

    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => $this->tag,
            'description' => 'Person resource'
        ));


        $this->addDefinition(array(
            'name' => 'Person.Record.Response.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Record.Response'
            )
        ));

        $this->addDefinition(array(
            'name' => 'Identification.Record.Response.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Identification.Record.Response'
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Record.Response',
            'type' => 'object',
            'properties' => array(
                'plusNumber' => array(
                    'type' => 'string',
                    'description' => 'Banner Plus Number for Person'
                ),
                'uniqueId' => array(
                    'type' => 'string',
                    'description' => 'Miami UniqueId for Person'
                ),
                'pidm' => array(
                    'type' => 'string',
                    'description' => 'pidm for Person'
                ),
                'ssn' => array(
                    'type' => 'string',
                    'description' => 'ssn for Person'
                ),
                'prefix' => array(
                    'type' => 'string',
                    'description' => 'Prefix for name'
                ),
                'suffix' => array(
                    'type' => 'string',
                    'description' => 'Suffix for name'
                ),
                'givenNameLegal' => array(
                    'type' => 'string',
                    'description' => 'Legal first name'
                ),
                'givenNamePreferred' => array(
                    'type' => 'string',
                    'description' => 'Preferred first name'
                ),
                'familyName' => array(
                    'type' => 'string',
                    'description' => 'Last name'
                ),
                'middleName' => array(
                    'type' => 'string',
                    'description' => 'Middle name'
                ),
                'age' => array(
                    'type' => 'integer',
                    'description' => 'Age for Person'
                ),
                'birthDate' => array(
                    'type' => 'string',
                    'description' => 'Birth date for Person'
                ),
                'emailAddress' => array(
                    'type' => 'string',
                    'description' => 'Email address for person'
                ),
                'genderCode' => array(
                    'type' => 'string',
                    'description' => 'Gender code for Person'
                ),
                'genderDescription' => array(
                    'type' => 'string',
                    'description' => 'Gender description for Person'
                ),
                'residencyStatus' => array(
                    'type' => 'string',
                    'description' => 'Residency status for Person'
                ),
                'termCode' => array(
                    'type' => 'string',
                    'description' => 'Current term code for this Person response'
                ),
                'studentTypeCode' => array(
                    'type' => 'string',
                    'description' => 'Student type code for this Person'
                )
            ),
        ));

        $this->addDefinition(array(
            'name' => 'Person.IdPhoto.Response',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'string',
                    'description' => 'Banner PIDM for Person'
                ),
                'uniqueId' => array(
                    'type' => 'string',
                    'description' => 'Miami UniqueId for Person'
                ),
                'jpeg' => array(
                    'type' => 'string',
                    'description' => 'Base64 encoded jpeg data'
                ),
            ),
        ));

        $this->addDefinition([
            'name' => 'Person.post.model',
            'type' => 'object',
            'properties' => [
                'pidm' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'ssn' => [
                    'type' => 'string',
                ],
                'birthDate' => [
                    'type' => 'string',
                ],
                'legacyCode' => [
                    'type' => 'string',
                ],
                'ethnicCode' => [
                    'type' => 'string',
                ],
                'maritalCode' => [
                    'type' => 'string',
                ],
                'sex' => [
                    'type' => 'string',
                ],
                'veteranIdNumber' => [
                    'type' => 'string',
                ],
                'confidentialIndicator' => [
                    'type' => 'string',
                ],
                'deceasedIndicator' => [
                    'type' => 'string',
                ],
                'deceasedDate' => [
                    'type' => 'string',
                ],
                'namePrefix' => [
                    'type' => 'string',
                ],
                'nameSuffix' => [
                    'type' => 'string',
                ],
                'preferredFirstName' => [
                    'type' => 'string',
                ],
                'citizenIndicator' => [
                    'type' => 'string',
                ],
                'veteranIndicator' => [
                    'type' => 'string',
                ],
                'armedForcesIndicator' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'governmentEthnicCode' => [
                    'type' => 'string',
                ],
                'genderCode' => [
                    'type' => 'string',
                ],
                'personalPronoun' => [
                    'type' => 'string',
                ],
            ]
        ]);

        $this->addDefinition([
            'name' => 'Person.put.model',
            'type' => 'object',
            'properties' => [
                'ssn' => [
                    'type' => 'string',
                ],
                'birthDate' => [
                    'type' => 'string',
                ],
                'legacyCode' => [
                    'type' => 'string',
                ],
                'ethnicCode' => [
                    'type' => 'string',
                ],
                'maritalCode' => [
                    'type' => 'string',
                ],
                'sex' => [
                    'type' => 'string',
                ],
                'veteranIdNumber' => [
                    'type' => 'string',
                ],
                'confidentialIndicator' => [
                    'type' => 'string',
                ],
                'deceasedIndicator' => [
                    'type' => 'string',
                ],
                'deceasedDate' => [
                    'type' => 'string',
                ],
                'namePrefix' => [
                    'type' => 'string',
                ],
                'nameSuffix' => [
                    'type' => 'string',
                ],
                'preferredFirstName' => [
                    'type' => 'string',
                ],
                'citizenIndicator' => [
                    'type' => 'string',
                ],
                'veteranIndicator' => [
                    'type' => 'string',
                ],
                'armedForcesIndicator' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'governmentEthnicCode' => [
                    'type' => 'string',
                ],
                'genderCode' => [
                    'type' => 'string',
                ],
                'personalPronoun' => [
                    'type' => 'string',
                ],
            ]
        ]);

        $this->addDefinition([
            'name' => 'Identification.Record.Response',
            'type' => 'object',
            'properties' => [
                'pidm' => [
                    'type' => 'string',
                    'description' => 'Pidm of the person'
                ],
                'muid' => [
                    'type' => 'string',
                    'description' => 'Unique ID, Plus Number or other ID of the person'
                ],
                'lastName' => [
                    'type' => 'string',
                    'description' => 'Last name of the person'
                ],
                'firstName' => [
                    'type' => 'string',
                    'description' => 'First Name of the person'
                ],
                'middleName' => [
                    'type' => 'string',
                    'description' => 'Middle Name of the person'
                ],
                'changeIndicator' => [
                    'type' => 'string',
                    'description' => 'Indicates whether the change made was ID number or name change'
                ],
                'entityIndicator' => [
                    'type' => 'string',
                    'description' => 'Identifies whether the record is person or non-person'
                ],
                'surrogateId' => [
                    'type' => 'string',
                    'description' => 'Immutable unique key'
                ],
                'additionalIds' => [
                    'type' => 'object',
                    'description' => ' Additional Identifications Collection',
                    '$ref' => '#/definitions/AdditionalIdentification.Collection'
                ]
            ]
        ]);

        $this->addDefinition(array(
            'name' => 'AdditionalIdentification',
            'type' => 'object',
            'properties' => array(
                'additionalIdCode' => array(
                    'type' => 'string',
                ),
                'additionalId' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'AdditionalIdentification.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/AdditionalIdentification'
            )
        ));

        $this->addDefinition(array(
            'name' => 'AdditionalIdentification.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/AdditionalIdentification'
            )
        ));

        $this->addDefinition(array(
            'name' => 'AdditionalIdentification.post',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'string',
                    'enum' => ['required|string']
                ),
                'additionalIdCode' => array(
                    'type' => 'string',
                    'enum' => ['required|string']
                ),
                'additionalId' => array(
                    'type' => 'string',
                    'enum' => ['required|string']
                )
            )
        ));


        $this->addDefinition([
            'name' => $this->name . '.post.model',
            'type' => 'object',
            'properties' => [
                'pidm' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'muid' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'lastName' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'firstName' => [
                    'type' => 'string',
                ],
                'middleName' => [
                    'type' => 'string',
                ],
                'changeIndicator' => [
                    'type' => 'string',
                    'enum' => ['required|I|N']
                ],
                'entityIndicator' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
            ]
        ]);

        $this->addDefinition([
            'name' => $this->name . '.put.model',
            'type' => 'object',
            'properties' => [
                'pidm' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'muid' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'lastName' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'firstName' => [
                    'type' => 'string',
                ],
                'middleName' => [
                    'type' => 'string',
                ],
                'changeIndicator' => [
                    'type' => 'string',
                ],
                'entityIndicator' => [
                    'type' => 'string',
                ]
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Person',
            'class' => 'MiamiOH\RestngPersonWebService\Services\Person',
            'description' => 'Provide CRUD interface to Person model',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'bannerUtil' => array(
                    'type' => 'service',
                    'name' => 'MU\BannerUtil'
                ),
            )
        ));

        $this->addService(array(
            'name' => 'UpdatePersonService',
            'class' => 'MiamiOH\RestngPersonWebService\Services\UpdatePersonService',
            'description' => 'Update Person Web Service',
            'set' => array(
                'databaseFactory' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                )
            )
        ));

        $this->addService([
            'name' => $this->name,
            'class' => Service::class,
            'description' => 'Provide database source',
        ]);

        $this->addService([
            'name' => 'Identification',
            'class' => IdentificationService::class,
            'description' => 'Provide CURD for Person Identifications',
        ]);

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.read.alternate',
            'description' => 'Get information of Person data from Banner.',
            'pattern' => '/person/v2/:personID',
            'service' => 'Person',
            'method' => 'getPersonsID',
            'isPartialable' => true,
            'returnType' => 'model',
            'tags' => array('Person'),

            'params' => array(
                'personID' => array(
                    'type' => 'list',
                    'description' => 'List of PersonID to query for',
                    'alternateKeys' => ['uniqueId', 'pidm', 'plus']
                ),
            ),
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'anonymous'
                    ),
                    array(
                        'type' => 'token'
                    )
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A Person collection',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.Record.Response'
                    )
                ),

                App::API_NOTFOUND => array(
                    'description' => 'The requested personID could not be found'
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'Unauthorized Access',
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.read.collection.v2',
            'description' => 'Get collection of Person data from Banner.',
            'pattern' => '/person/v2',
            'service' => 'Person',
            'method' => 'getPersons',
            'returnType' => 'collection',
            'isPartialable' => false,
            'tags' => array('Person'),
            'options' => array(
                'pidm' => array(
                    'type' => 'list',
                    'description' => 'List of pidms to query for'
                ),
                'uniqueId' => array(
                    'type' => 'list',
                    'description' => 'List of uniqueIds to query for'
                ),
                'familyName' => array(
                    'type' => 'list',
                    'description' => 'List of Last Name to query for'
                ),
                'givenNameLegal' => array(
                    'type' => 'list',
                    'description' => 'List of First Name to query for'
                ),

            ),
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'anonymous'
                    ),
                    array(
                        'type' => 'token'
                    )
                ),

            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A Person collection',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Person.Record.Response.Collection'

                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'The requested pidm and uniqueId could not be found'
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'Unauthorized Access - Display Anonymous Information',
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.byMajor.read.collection.v3',
            'description' => 'Get collection of Person data from Banner.',
            'pattern' => '/person/v3/major',
            'service' => 'Person',
            'method' => 'getByMajors',
            'returnType' => 'collection',
            'isPageable' => true,
            'defaultPageLimit' => 100,
            'maxPageLimit' => 500,
            'tags' => array('Person'),
            'options' => array(
                'majorCodes' => array(
                    'type' => 'list',
                    'description' => 'List of major codes'
                )
            ),
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'anonymous'
                    ),
                    array(
                        'type' => 'token'
                    )
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A Person collection',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Person.Record.Response.Collection'

                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'The requested pidm and uniqueId could not be found'
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'Unauthorized Access - Display Anonymous Information',
                )
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'person.v3.update',
            'description' => 'Update person information by uniqueId. In order to use this resource, you must have `All`, `Protected` or `Publid` key in the Authman application `WebServices` (Module: `Person`)',
            'summary' => 'Update person information',
            'pattern' => '/person/v3',
            'service' => 'UpdatePersonService',
            'method' => 'update',
            'returnType' => 'model',
            'tags' => array('Person'),
            'options' => array(
                'uniqueId' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'UniqueID'
                ],
                'preferredFirstName' => [
                    'type' => 'single',
                    'description' => 'preferred first name'
                ],
                'directoryListing' => [
                    'type' => 'single',
                    'description' => 'directory listing',
                    'enum' => [
                        'L',
                        'U'
                    ]
                ],
                'hideMiddleName' => [
                    'type' => 'single',
                    'description' => 'hide middle name flag',
                    'enum' => [
                        'true',
                        'false'
                    ]
                ],
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'All'
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'Protected'
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'Public'
                    ),
                ),
            ),
            'responses' => [
                App::API_OK => [
                    'description' => 'A Person collection',
                    'returns' => [
                        'type' => 'object',
                        'properties' => []
                    ]
                ],
                App::API_BADREQUEST => [
                    'description' => 'bad request',
                ]
            ]
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.read.collection.v3',
            'description' => 'Get collection of Person data from Banner.',
            'pattern' => '/person/v3',
            'service' => 'Person',
            'method' => 'getPersons',
            'returnType' => 'collection',
            'isPartialable' => false,
            'isPageable' => true,
            'defaultPageLimit' => 100,
            'maxPageLimit' => 500,
            'tags' => array('Person'),
            'options' => array(
                'pidm' => array(
                    'type' => 'list',
                    'description' => 'List of pidms to query for'
                ),
                'uniqueId' => array(
                    'type' => 'list',
                    'description' => 'List of uniqueIds to query for'
                ),
                'bannerPlusId' => array(
                    'type' => 'list',
                    'description' => 'List of bannerPlusIds to query for. Add + symbol'
                ),
                'familyName' => array(
                    'type' => 'list',
                    'description' => 'List of Last Name to query for'
                ),
                'givenNameLegal' => array(
                    'type' => 'list',
                    'description' => 'List of First Name to query for'
                ),
                'effectiveTermCode' => array(
                    'type' => 'single',
                    'description' => 'The term code we wish to use for inclusion in population, 6 digits. Defaults to current term code, if missing. This is ONLY used if the population is activeEligibleStudents or currentActiveResidentStudents'
                ),
                'population' => array(
                    'description' => "\n The population of people to query for 
                      \n##### Available Options:
                      \n- enrollmentEligibleStudents: Students who have sgbstdn record for the current term or future terms.
                      \n- currentActiveStudents: Students who have an active sgbstdn record for the current term.
                      \n- activeEligibleStudents: Students that have at least one active (‘AS’) or non-credit ('NC') sgbstdn record in any term beginning from the specified effectiveTermCode. 
                      \n- activeEmployees: Employees who have records in Pebempl that do not have status of terminated.
                      \n- emeriti: People who have a record in aprpros with a purpose type of SOME, SW or SSP.
                      \n- retirees: People who have a record in aprpros with a purpose type of SOMR.
                      \n- inactiveEmployees: Employees that have records in Pebempl that do  have status of terminated.
                      \n- employeesWorkedRollingLastYear: Employees that have records in Pebempl that have a last work date of null or has been within the last year of the current date.
                      \n- alumni: People who have a record in aprcatg and their donor code(aprcatg_donr_code) has an alumni indicator of Y.
                      \n- alumniRollingTwoYears: People who have a record in aprcatg and their donor code(aprcatg_donr_code) has an alumni indicator of Y and who were awarded a degree within the last two years of the current date.
                      \n- currentActiveResidentStudents: Students who have an active sgbstdn record for the current term and who are a resident. 
                      ",

                    'enum' => [
                        'enrollmentEligibleStudents',
                        'currentActiveStudents',
                        'activeEligibleStudents',
                        'activeEmployees',
                        'emeriti',
                        'retirees',
                        'inactiveEmployees',
                        'employeesWorkedRollingLastYear',
                        'alumni',
                        'alumniRollingTwoYears',
                        'currentActiveResidentStudents'
                    ]
                )
            ),
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'anonymous'
                    ),
                    array(
                        'type' => 'token'
                    )
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A Person collection',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Person.Record.Response.Collection'

                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'The requested pidm and uniqueId could not be found'
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'Unauthorized Access - Display Anonymous Information',
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.v3.read.alternate',
            'description' => 'Get information of Person data from Banner.',
            'pattern' => '/person/v3/:personID',
            'service' => 'Person',
            'method' => 'getPersonsID',
            'isPartialable' => true,
            'returnType' => 'model',
            'tags' => array('Person'),

            'params' => array(
                'personID' => array(
                    'type' => 'list',
                    'description' => 'List of PersonID to query for',
                    'alternateKeys' => ['uniqueId', 'pidm', 'plus']
                ),
            ),
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'anonymous'
                    ),
                    array(
                        'type' => 'token'
                    )
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A Person collection',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.Record.Response'
                    )
                ),

                App::API_NOTFOUND => array(
                    'description' => 'The requested personID could not be found'
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'Unauthorized Access',
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.idphoto.read',
            'description' => 'Get photo ID for person',
            'pattern' => '/person/v2/idPhoto/:muid',
            'service' => 'Person',
            'method' => 'getPersonIdPhoto',
            'isPartialable' => false,
            'returnType' => 'model',
            'tags' => array('Person'),
            'params' => array(
                'muid' => array(
                    'description' => 'A uniqueid',
                    'alternateKeys' => ['uniqueId', 'pidm']
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-Photo',
                        'key' => 'view'
                    ),
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A Person photo ID response',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.IdPhoto.Response'
                    )
                ),

                App::API_NOTFOUND => array(
                    'description' => 'The requested personID could not be found'
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'Unauthorized Access',
                )
            )
        ));

        $this->addResource([
            'action' => 'create',
            'description' => 'Create new person record',
            'name' => '.post.single',
            'service' => $this->name,
            'method' => 'postSingle',
            'tags' => ['Person'],
            'pattern' => '/person/v4',
            'body' => [
                'required' => true,
                'description' => 'Person Data',
                'schema' => [
                    '$ref' => '#/definitions/' . 'Person.post.model'
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => 'WebServices',
                    'module' => $this->tag,
                    'key' => ['create', 'all']
                ],
            ]
        ]);


        $this->addResource([
            'action' => 'update',
            'description' => 'Update person data',
            'name' => $this->name . '.put.single',
            'service' => $this->name,
            'method' => 'putSingle',
            'tags' => ['Person'],
            'pattern' => '/person/v4/:pidm',
            'params' => [
                'pidm' => ['description' => 'Pidm for the record being updated']
            ],
            'body' => [
                'required' => true,
                'description' => 'Person data',
                'schema' => [
                    '$ref' => '#/definitions/' . 'Person.put.model'
                ]
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'Update successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Update operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => 'WebServices',
                    'module' => $this->tag,
                    'key' => ['update', 'edit', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'read',
            'description' => 'Get person identification records',
            'name' => 'Identification.read.collection',
            'service' => 'Identification',
            'method' => 'getIdentification',
            'returnType' => 'collection',
            'tags' => ['Person'],
            'pattern' => '/person/identification/v1',
            'options' => [
                'pidm' => [
                    'type' => 'list',
                    'description' => 'Return the identification records for the particular pidm'
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A collection of employee identification records',
                    'returns' => [
                        'type' => 'array',
                        '$ref' => '#/definitions/Identification.Record.Response',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'There is no record found.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => 'WebServices',
                    'module' => $this->tag,
                    'key' => ['view', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'create',
            'description' => 'Create new identification record',
            'name' => 'Identification.post.single',
            'service' => 'Identification',
            'method' => 'postSingle',
            'tags' => ['Person'],
            'pattern' => '/person/identification/v1',
            'body' => [
                'required' => true,
                'description' => 'Identification Data',
                'schema' => [
                    '$ref' => '#/definitions/' . $this->name . '.post.model'
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_NOTFOUND => [
                    'description' => 'Underlying record not found.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => 'WebServices',
                    'module' => $this->tag,
                    'key' => ['create', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'update',
            'description' => 'Update identification data',
            'name' =>  'Identification.put.single',
            'service' => 'Identification',
            'method' => 'putSingle',
            'tags' => ['Person'],
            'pattern' => '/person/identification/v1/:surrogateId',
            'params' => array(
                'surrogateId' => array('description' => 'Surrogate ID for the record that is being updated'),
            ),
            'body' => [
                'required' => true,
                'description' => 'Identification data',
                'schema' => [
                    '$ref' => '#/definitions/' . $this->name . '.put.model'
                ]
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'Update successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_NOTFOUND => [
                    'description' => 'Record not found.',
                ],
                App::API_FAILED => [
                    'description' => 'Update operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => 'WebServices',
                    'module' => $this->tag,
                    'key' => ['update', 'edit', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'create',
            'description' => 'Create new Additional identification record',
            'name' => 'AdditionalIdentification.post.single',
            'service' => 'Identification',
            'method' => 'postSingleAdditionalIdentification',
            'tags' => ['Person'],
            'pattern' => '/person/additionalIdentification/v1',
            'body' => [
                'required' => true,
                'description' => 'Creates Additional Identification Data',
                'schema' => [
                    '$ref' => '#/definitions/AdditionalIdentification.post'
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_NOTFOUND => [
                    'description' => 'Underlying record not found.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => 'WebServices',
                    'module' => $this->tag,
                    'key' => ['create', 'all']
                ],
            ]
        ]);
    }

    public function registerOrmConnections(): void
    { }
}
