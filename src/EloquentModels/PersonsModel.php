<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 1/29/19
 * Time: 2:17 PM
 */

namespace MiamiOH\RestngPersonWebService\EloquentModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Query\OracleBuilder;

class PersonsModel extends Model
{
    /**
     * @var string
     */
    protected $connection = 'MUWS_SEC_PROD';

    /**
     * @var string $table Table name
     */
    public $table = 'spbpers';

    /**
     * @var bool $timestamps Do not populate auto-generated date fields
     */
    public $timestamps = false;

    /**
     * @var bool $incrementing Do not increment primary key by default
     */
    public $incrementing = false;

    /**
     * @var string $primaryKey Primary key of table
     */
    protected $primaryKey = 'spbpers_pidm';

    /**
     * @var array $guarded black list of insertable fields
     */
    protected $guarded = [];

    /**
     * Get a new query builder instance for the connection.
     * https://github.com/yajra/laravel-oci8/issues/73#issuecomment-117131744
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();

        return new OracleBuilder($conn, $grammar, $conn->getPostProcessor());
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeBuildSelect(Builder $query): Builder
    {
        return $query->select(
            'spbpers_pidm AS pidm',
            'spbpers_ssn AS ssn',
            'spbpers_birth_date AS birth_date',
            'spbpers_lgcy_code AS legacy_code',
            'spbpers_ethn_code AS ethnic_code',
            'spbpers_mrtl_code AS marital_code',
            'spbpers_sex AS gender',
            'spbpers_activity_date AS activity_date',
            'spbpers_vetc_file_number AS veteran_id_number',
            'spriden_legal_name AS legal_name',
            'spriden_pref_first_name AS preferred_first_name',
            'spriden_name_prefix AS name_prefix',
            'spriden_name_suffix AS name_suffix',
            'spbpers_confid_ind AS confidential_indicator',
            'spbpers_dead_ind AS deceased_indicator',
            'spbpers_dead_date AS deceased_date',
            'spbpers_vera_ind AS veteran_indicator',
            'spbpers_citz_ind AS citizen_indicator',
            'spbpers_gndr_code AS gender_code',
            'spbpers_ethn_cde AS government_ethnic_code',
            'spbpers_pprn_code AS personal_pronoun'
        );
    }

    /**
     * @param Builder $query
     * @param string $pidm
     * @return Builder
     */
    public function scopeWherePidm(Builder $query, string $pidm): Builder
    {
        if (!empty($pidm)) {
            $query->where('spbpers_pidm', $pidm);
        }
        return $query;
    }


}