<?php

namespace MiamiOH\RestngPersonWebService\Objects;

class AdditionalIdentification
{
    /**
     * 
     * @var string
     */
    private $additionalIdCode;
    /**
     *
     * @var string
     */
    private $additionalId;
    /**
     *
     * @param string $additionalIdCode
     * @param string $additionalId
     * @return void
     */
    public function __construct(string $additionalIdCode, string $additionalId)
    {
        $this->additionalIdCode = $additionalIdCode;
        $this->additionalId = $additionalId;
    }

    /**
     * return additional identification code
     *
     * @return string
     */
    public function getAdditionalIdCode(): string
    {
        return $this->additionalIdCode;
    }

    /**
     * return additional identification
     *
     * @return string
     */
    public function getAdditionalId(): string
    {
        return $this->additionalId;
    }

    /**
     * 
     * @return array
     */
    public function toJsonArray(): array
    {
        return [
            'additionalIdCode' => $this->getAdditionalIdCode(),
            'additionalId' => $this->getAdditionalId()
        ];
    }
    
}
