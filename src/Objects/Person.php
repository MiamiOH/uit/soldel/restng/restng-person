<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 01/29/19
 * Time: 2:59 PM
 */

namespace MiamiOH\RestngPersonWebService\Objects;

use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;


/**
 * Class Person
 * @package MiamiOH\RestngPersonWebService\Objects
 */
class Person
{
    /**
     * @var string
     */
    private $pidm= '';

    /**
     * @var string
     */
    private $ssn = '';

    /**
     * @var string
     */
    private $legacyCode = '';

    /**
     * @var string
     */
    private $ethnicCode = '';

    /**
     * @var string
     */
    private $maritalCode = '';

    /**
     * @var string
     */
    private $sex = '';

    /**
     * @var string
     */
    private $veteranIdNumber= '';

    /**
     * @var string
     */
    private $confidentialIndicator = '';

    /**
     * @var string
     */
    private $deceasedIndicator = '';

    /**
     * @var string
     */
    private $deceasedDate = '';

    /**
     * @var string
     */
    private $veteranIndicator = '';

    /**
     * @var string
     */
    private $citizenIndicator = '';

    /**
     * @var string
     */
    private $armedForcesIndicator = '';

    /**
     * @var string
     */
    private $birthDate = '';

    /**
     * @var string
     */
    private $legalName = '';

    /**
     * @var string
     */
    private $preferredFirstName = '';

    /**
     * @var string
     */
    private $namePrefix = '';

    /**
     * @var string
     */
    private $nameSuffix= '';

    /**
     * @var string
     */
    private $governmentEthnicCode = '';

    /**
     * @var string
     */
    private $genderCode= '';

    /**
     * @var string
     */
    private $personalPronoun= '';

    /**
     * @var string
     */
    private $userId = '';

    /**
     * @var string
     */
    private $dataOrigin = '';

    /**
     * @var string
     */
    private $residencyStatus = '';

    /**
     * @return string
     */
    public function getPidm(): string
    {
        return $this->pidm;
    }

    /**
     * @param string $pidm
     * @return Person
     */
    public function setPidm(string $pidm): Person
    {
        $this->pidm= $pidm;
        return $this;
    }

    /**
     * @return string
     */
    public function getSsn(): string
    {
        return $this->ssn;
    }

    /**
     * @param string $ssn
     * @return Person
     */
    public function setSsn(string $ssn): Person
    {
        $this->ssn= $ssn;
        return $this;
    }

    /**
     * @return string
     */
    public function getLegacyCode(): string
    {
        return $this->legacyCode;
    }

    /**
     * @param string $legacyCode
     * @return Person
     */
    public function setLegacyCode(string $legacyCode): Person
    {
        $this->legacyCode= $legacyCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getEthnicCode(): string
    {
        return $this->ethnicCode;
    }

    /**
     * @param string $ethnicCode
     * @return Person
     */
    public function setEthnicCode(string $ethnicCode): Person
    {
        $this->ethnicCode= $ethnicCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getMaritalCode(): string
    {
        return $this->maritalCode;
    }

    /**
     * @param string $maritalCode
     * @return Person
     */
    public function setMaritalCode(string $maritalCode): Person
    {
        $this->maritalCode= $maritalCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getSex(): string
    {
        return $this->sex;
    }

    /**
     * @param string $sex
     * @return Person
     */
    public function setSex(string $sex): Person
    {
        $this->sex= $sex;
        return $this;
    }

    /**
     * @return string
     */
    public function getConfidentialIndicator(): string
    {
        return $this->confidentialIndicator;
    }

    /**
     * @param string $confidentialIndicator
     * @return Person
     */
    public function setConfidentialIndicator(string $confidentialIndicator): Person
    {
        $this->confidentialIndicator= $confidentialIndicator;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeceasedIndicator(): string
    {
        return $this->deceasedIndicator;
    }

    /**
     * @param string $deceasedIndicator
     * @return Person
     */
    public function setDeceasedIndicator(string $deceasedIndicator): Person
    {
        $this->deceasedIndicator= $deceasedIndicator;
        return $this;
    }


    /**
     * @return string
     */
    public function getVeteranIndicator(): string
    {
        return $this->veteranIndicator;
    }

    /**
     * @param string $veteranIndicator
     * @return Person
     */
    public function setVeteranIndicator(string $veteranIndicator): Person
    {
        $this->veteranIndicator= $veteranIndicator;
        return $this;
    }


    /**
     * @return string
     */
    public function getCitizenIndicator(): string
    {
        return $this->citizenIndicator;
    }

    /**
     * @param string $citizenIndicator
     * @return Person
     */
    public function setCitizenIndicator(string $citizenIndicator): Person
    {
        $this->citizenIndicator= $citizenIndicator;
        return $this;
    }

    /**
     * @return string
     */
    public function getBirthDate(string $format = 'Y-m-d H:i:s'): string
    {
        if(empty($this->birthDate)){
            return '';
        }
        return $this->birthDate->format($format);
    }

    /**
     * @param string $birthDate
     * @return Person
     */
    public function setBirthDate(string $birthDate): Person
    {
        if(empty($birthDate)){
            $this->birthDate = null;
        } else {
            $this->birthDate = new \DateTime($birthDate);
        }
        return $this;
    }


    /**
     * @return string
     */
    public function getDeceasedDate(string $format = 'Y-m-d H:i:s'): string
    {
        if(empty($this->deceasedDate)){
            return '';
        }
        return $this->deceasedDate->format($format);
    }

    /**
     * @param string $deceasedDate
     * @return Person
     */
    public function setDeceasedDate(string $deceasedDate): Person
    {
        if(empty($deceasedDate)){
            $this->deceasedDate = null;
        } else {
            $this->deceasedDate= new \DateTime($deceasedDate);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getVeteranIdNumber(): string
    {
        return $this->veteranIdNumber;
    }

    /**
     * @param string $veteranIdNumber
     * @return Person
     */
    public function setVeteranIdNumber(string $veteranIdNumber): Person
    {
        $this->veteranIdNumber= $veteranIdNumber;
        return $this;
    }


    /**
     * @return string
     */
    public function getPreferredFirstName(): string
    {
        return $this->preferredFirstName;
    }

    /**
     * @param string $preferredFirstName
     * @return Person
     */
    public function setPreferredFirstName(string $preferredFirstName): Person
    {
        $this->preferredFirstName= $preferredFirstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLegalName(): string
    {
        return $this->legalName;
    }

    /**
     * @param string $legalName
     * @return Person
     */
    public function setLegalName(string $legalName): Person
    {
        $this->legalName= $legalName;
        return $this;
    }

    /**
     * @return string
     */
    public function getNamePrefix(): string
    {
        return $this->namePrefix;
    }

    /**
     * @param string $namePrefix
     * @return Person
     */
    public function setNamePrefix(string $namePrefix): Person
    {
        $this->namePrefix= $namePrefix;
        return $this;
    }

    /**
     * @return string
     */
    public function getNameSuffix(): string
    {
        return $this->nameSuffix;
    }

    /**
     * @param string $nameSuffix
     * @return Person
     */
    public function setNameSuffix(string $nameSuffix): Person
    {
        $this->nameSuffix= $nameSuffix;
        return $this;
    }

    /**
     * @return string
     */
    public function getGovernmentEthnicCode(): string
    {
        return $this->governmentEthnicCode;
    }

    /**
     * @param string $governmentEthnicCode
     * @return Person
     */
    public function setGovernmentEthnicCode(string $governmentEthnicCode): Person
    {
        $this->governmentEthnicCode= $governmentEthnicCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getGenderCode(): string
    {
        return $this->genderCode;
    }

    /**
     * @param string $genderCode
     * @return Person
     */
    public function setGenderCode(string $genderCode): Person
    {
        $this->genderCode= $genderCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getPersonalPronoun(): string
    {
        return $this->personalPronoun;
    }

    /**
     * @param string $personalPronoun
     * @return Person
     */
    public function setPersonalPronoun(string $personalPronoun): Person
    {
        $this->personalPronoun= $personalPronoun;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getActivityDate(): \DateTime
    {
        return new \DateTime('now');
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return Person
     */
    public function setUserId(string $userId): Person
    {
        $this->userId = $userId;
        return $this;
    }


    /**
     * @return string
     */
    public function getDataOrigin(): string
    {
        return $this->dataOrigin;
    }

    /**
     * @param string $dataOrigin
     * @return Person
     */
    public function setDataOrigin(string $dataOrigin): Person
    {
        $this->dataOrigin = $dataOrigin;
        return $this;
    }

    /**
     * @return string
     */
    public function getArmedForcesIndicator(): string
    {
        return $this->armedForcesIndicator;
    }

    /**
     * @param string $armedForcesIndicator
     * @return Person
     */
    public function setArmedForcesIndicator(string $armedForcesIndicator): Person
    {
        $this->armedForcesIndicator= $armedForcesIndicator;
        return $this;
    }

    /**
     * @return string
     */
    public function getResidencyStatus(): string
    {
        return $this->residencyStatus;
    }

    /**
     * @param string $residencyStatus
     * @return $this
     */
    public function setResidencyStatus(string $residencyStatus): Person
    {
        $this->residencyStatus = $residencyStatus;
        return $this;
    }

    /**
     * @var array
     */
    private static $rules = [
        'pidm' => ['required', 'regex:/^\d{1,8}$/'], // delegate validation to ws-muid
        'ssn' => ['max:15'],
        'legacyCode' => ['max:1'],
        'ethnicCode' => ['max:2'],
        'maritalCode' => ['max:1'],
        'sex' => ['max:1'],
        'confidentialIndicator' => ['max:1'],
        'deceasedIndicator' => ['max:1'],
        'namePrefix' => ['max:20'],
        'nameSuffix' => ['max:20'],
        'preferredFirstName' => ['max:60'],
        'citizenIndicator' => ['max:1'],
        'veteranIndicator' => ['max:1'],
        'governmentEthnicCode' => ['max:1'],
        'armedForcesIndicator' => ['required','max:1'],
        'genderCode' => ['max:4'],
        'personalPronoun' => ['max:4'],
        'userId' => ['max:30'],
        'dataOrigin' => ['max:30'],
        'residencyStatus' => ['max:1']
    ];

    private $modifiedAttributes = [];

    /**
     * @return array
     */
    public function getModifiedAttributes(): array
    {
        return $this->modifiedAttributes;
    }

    /**
     * @param string $attributes
     */
    public function addModifiedAttributes(string $attributes): void
    {
        $this->modifiedAttributes[$attributes] = true;
    }

    /**
     * @param array $data
     * @return Identification
     * @throws \Exception
     */
    public static function fromArray(array $data): self
    {
        self::validateArray($data);

        $thisInstance = new self();

        try {
            foreach ($data as $key => $val) {
                $thisInstance->{'set' . ucfirst($key)}($val);
                $thisInstance->addModifiedAttributes($key);
            }
        } catch (\Exception $e) {
            throw new \Exception("Invalid key for Person: $key" . $e->getMessage());
        }

        return $thisInstance;
    }

    /**
     * @param array $data
     * @throws \Exception
     */
    public static function validateArray(array $data): void
    {
        $validator = RESTngValidatorFactory::make($data, self::$rules);

        if ($validator->fails()) {
            throw new \Exception(
                'Validation failed. '
                . implode(' ', $validator->errors()->all())
            );
        }
    }
}