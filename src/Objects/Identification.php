<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 01/29/19
 * Time: 2:39 PM
 */

namespace MiamiOH\RestngPersonWebService\Objects;

use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;
use MiamiOH\RestngPersonWebService\EloquentModels\IdentificationModel;

/**
 * Class Identification
 * @package MiamiOH\RestngPersonWebService\Objects
 */
class Identification
{
    /**
     * @var string
     */
    private $pidm= '';

    /**
     * @var string
     */
    private $muid = '';

    /**
     * @var string
     */
    private $lastName = '';

    /**
     * @var string
     */
    private $firstName = '';

    /**
     * @var string
     */
    private $middleName = '';

    /**
     * @var string
     */
    private $changeIndicator = '';

    /**
     * @var string
     */
    private $entityIndicator = '';

    /**
     * @var string
     */
    private $userId = '';

    /**
     * @var string
     */
    private $dataOrigin = '';

    /**
     * @var string
     */
    private $surrogateId = '';


    /**
     * @return string
     */
    public function getPidm(): string
    {
        return $this->pidm;
    }

    /**
     * @param string $pidm
     * @return Identification
     */
    public function setPidm(string $pidm): Identification
    {
        $this->pidm= $pidm;
        return $this;
    }

    /**
     * @return string
     */
    public function getMuid(): string
    {
        return $this->muid;
    }

    /**
     * @param string $muid
     * @return Identification
     */
    public function setMuid(string $muid): Identification
    {
        $this->muid= $muid;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Identification
     */
    public function setLastName(string $lastName): Identification
    {
        $this->lastName= $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Identification
     */
    public function setFirstName(string $firstName): Identification
    {
        $this->firstName= $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     * @return Identification
     */
    public function setMiddleName(string $middleName): Identification
    {
        $this->middleName= $middleName;
        return $this;
    }

    /**
     * @return string
     */
    public function getChangeIndicator(): string
    {
        return $this->changeIndicator;
    }

    /**
     * @param string $changeIndicator
     * @return Identification
     */
    public function setChangeIndicator(string $changeIndicator): Identification
    {
        $this->changeIndicator= $changeIndicator;
        return $this;
    }

    /**
     * @return string
     */
    public function getEntityIndicator(): string
    {
        return $this->entityIndicator;
    }

    /**
     * @param string $entityIndicator
     * @return Identification
     */
    public function setEntityIndicator(string $entityIndicator): Identification
    {
        $this->entityIndicator= $entityIndicator;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getActivityDate(): \DateTime
    {
        return new \DateTime('now');
    }


    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return Identification
     */
    public function setUserId(string $userId): Identification
    {
        $this->userId = $userId;
        return $this;
    }


    /**
     * @return string
     */
    public function getDataOrigin(): string
    {
        return $this->dataOrigin;
    }

    /**
     * @param string $dataOrigin
     * @return Identification
     */
    public function setDataOrigin(string $dataOrigin): Identification
    {
        $this->dataOrigin = $dataOrigin;
        return $this;
    }


    /**
     * @return string
     */
    public function getSurrogateId(): string
    {
        return $this->surrogateId;
    }

    /**
     * @param string $surrogateId
     * @return Identification
     */
    public function setSurrogateId(string $surrogateId): Identification
    {
        $this->surrogateId= $surrogateId;
        return $this;
    }


    /**
     * @var array
     */
    private static $rules = [
        'pidm' => ['required', 'regex:/^\d{1,8}$/'], // delegate validation to ws-muid
        'muid' => ['required', 'max:9'],
        'lastName' => ['required', 'max:60'],
        'firstName' => ['max:60'],
        'middleName' => ['max:60'],
        'changeIndicator' => ['required','max:1','regex:/^I|N$/'],
        'entityIndicator' => ['required','max:1'],
        'userId' => ['max:30'],
        'dataOrigin' => ['max:30'],
    ];

    private $modifiedAttributes = [];

    /**
     * @return array
     */
    public function getModifiedAttributes(): array
    {
        return $this->modifiedAttributes;
    }

    /**
     * @param string $attributes
     */
    public function addModifiedAttributes(string $attributes): void
    {
        $this->modifiedAttributes[$attributes] = true;
    }

    /**
     * @param array $data
     * @return Identification
     * @throws \Exception
     */
    public static function fromArray(array $data): self
    {
        self::validateArray($data);

        $thisInstance = new self();

        try {
            foreach ($data as $key => $val) {
                $thisInstance->{'set' . ucfirst($key)}($val);
                $thisInstance->addModifiedAttributes($key);
            }
        } catch (\Exception $e) {
            throw new \Exception("Invalid key for Identification: $key" . $e->getMessage());
        }

        return $thisInstance;
    }

    /**
     * @param array $data
     * @throws \Exception
     */
    public static function validateArray(array $data): void
    {
        $validator = RESTngValidatorFactory::make($data, self::$rules);

        if ($validator->fails()) {
            throw new \Exception(
                'Validation failed. '
                . implode(' ', $validator->errors()->all())
            );
        }
    }
}
