<?php
/**
 * Author: liaom
 * Date: 7/30/18
 * Time: 11:38 AM
 */

namespace MiamiOH\RestngPersonWebService\Tests\Unit;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Legacy\DataSource;
use MiamiOH\RESTng\Legacy\DB\DBH;
use MiamiOH\RESTng\Testing\TestCase;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RestngPersonWebService\Services\Identification\IdentificationService;
use MiamiOH\RestngPersonWebService\Services\Person;
use PHPUnit\Framework\MockObject\MockObject;

class PostAdditionnalIdentificationTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $request;

    /**
     * @var MockObject
     */
    private $response;
    /**
     * @var MockObject
     */
    private $dbh;
    /**
     * @var IdentificationService
     */
    private $identificationService;
    /**
     * @var MockObject
     */
    private $apiUser;

    /**
     * @var DatabaseFactory
     */
    private $databaseFactory;

    public function testMissingPidm()
    {

        $this->request->method('getOptions')->willReturn([
            'additionalIdCode' => 'abc',
            'additionalId' => 'cbd'
        ]);

        $response = $this->identificationService->postSingle();
        $this->assertEquals(400, $response->getStatus());
    }

    public function testMissingAdditionalIdCode()
    {

        $this->request->method('getOptions')->willReturn([
            'pidm' => '1440394',
            'additionalId' => 'cbd'        ]);

        $response = $this->identificationService->postSingle();
        $this->assertEquals(400, $response->getStatus());
    }

    public function testMissingAdditionalId()
    {

        $this->request->method('getOptions')->willReturn([
            'pidm' => '1440394',
            'additionalIdCode' => 'abc',
        ]);

        $response = $this->identificationService->postSingle();
        $this->assertEquals(400, $response->getStatus());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->request = $this->createMock(Request::class);
        $this->response = $this->createMock(Response::class);
        $this->apiUser = $this->createMock(User::class);
        $this->identificationService = new IdentificationService();
        $this->identificationService->setRequest($this->request);
        $this->identificationService->setApiUser($this->apiUser);
    }
}