<?php
/**
 * Author: liaom
 * Date: 7/30/18
 * Time: 11:38 AM
 */

namespace MiamiOH\RestngPersonWebService\Tests\Unit;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Legacy\DB\DBH;
use MiamiOH\RESTng\Testing\TestCase;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RestngPersonWebService\Services\Person;
use PHPUnit\Framework\MockObject\MockObject;

class GetPersonTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $request;
    /**
     * @var MockObject
     */
    private $dbh;
    /**
     * @var Person
     */
    private $personService;
    /**
     * @var MockObject
     */
    private $apiUser;

    public function testName()
    {
        $statement = $this->createMock(\MiamiOH\RESTng\Legacy\DB\STH\OCI8::class);
        $this->dbh->method('prepare')->willReturn($statement);
        $statement->method('execute')->willReturnSelf();
        $statement
            ->expects($this->at(1))
            ->method('fetchrow_assoc')
            ->willReturn([
                "szbuniq_banner_id" => "+01422366",
                "szbuniq_unique_id" => "DUSINGAR",
                "spriden_pidm" => "1440396",
                "spbpers_ssn" => "00000000",
                "name_prefix" => " ",
                "spriden_first_name" => "Andrew",
                "spbpers_pref_first_name" => null,
                "name_middle" => "Raymond",
                "spriden_last_name" => "Dusing",
                "name_suffix" => " ",
                "birth_date" => "1994-09-06",
                "email_add" => "dusingar@miamioh.edu",
                "gzbausr_hide_middle_name" => null,
                "spbpers_sex" => "M",
                "privacy_status" => "N",
                "rnum" => "1"
            ]);

        $statement
            ->expects($this->at(2))
            ->method('fetchrow_assoc')
            ->willReturn(false);

        $this->request->method('getOptions')->willReturn([
            'pidm' => ['1440394']
        ]);

        $response = $this->personService->getPersons();

        $this->assertEquals('Andrew Raymond Dusing',
            $response->getPayload()[0]['informalDisplayedName']);
        $this->assertEquals(' Andrew Raymond Dusing',
            $response->getPayload()[0]['formalDisplayedName']);
        $this->assertEquals('Dusing, Andrew Raymond',
            $response->getPayload()[0]['informalSortedName']);
        $this->assertEquals('Dusing, Andrew Raymond ',
            $response->getPayload()[0]['formalSortedName']);
    }

    public function testGetHideMiddleName()
    {
        $statement = $this->createMock(\MiamiOH\RESTng\Legacy\DB\STH\OCI8::class);
        $this->dbh->method('prepare')->willReturn($statement);
        $statement->method('execute')->willReturnSelf();
        $statement
            ->expects($this->at(1))
            ->method('fetchrow_assoc')
            ->willReturn([
                "szbuniq_banner_id" => "+01422366",
                "szbuniq_unique_id" => "DUSINGAR",
                "spriden_pidm" => "1440396",
                "spbpers_ssn" => "00000000",
                "name_prefix" => " ",
                "spriden_first_name" => "Andrew",
                "spbpers_pref_first_name" => null,
                "name_middle" => "Raymond",
                "spriden_last_name" => "Dusing",
                "name_suffix" => " ",
                "birth_date" => "1994-09-06",
                "email_add" => "dusingar@miamioh.edu",
                "gzbausr_hide_middle_name" => 'Y',
                "spbpers_sex" => "M",
                "privacy_status" => "N",
                "rnum" => "1"
            ]);

        $statement
            ->expects($this->at(2))
            ->method('fetchrow_assoc')
            ->willReturn(false);

        $this->request->method('getOptions')->willReturn([
            'pidm' => ['1440394']
        ]);

        $response = $this->personService->getPersons();

        $this->assertTrue($response->getPayload()[0]['hideMiddleName']);
    }

    public function testCanGetSensitiveInformation():void
    {
        $statement = $this->createMock(\MiamiOH\RESTng\Legacy\DB\STH\OCI8::class);
        $this->dbh->method('prepare')->willReturn($statement);
        $this->apiUser->method('isAuthenticated')->willReturn(true);
        $this->apiUser->expects($this->at(4))->method('isAuthorized')->with($this->equalTo('WebServices'),$this->equalTo('Person'),
        $this->equalTo('viewAllWithSsn'))->willReturn(true);
        $statement->method('execute')->willReturnSelf();
        $statement
            ->expects($this->at(1))
            ->method('fetchrow_assoc')
            ->willReturn([
                "szbuniq_banner_id" => "+01422366",
                "szbuniq_unique_id" => "DUSINGAR",
                "spriden_pidm" => "1440396",
                "spbpers_ssn" => "00000000",
                "name_prefix" => " ",
                "spriden_first_name" => "Andrew",
                "spbpers_pref_first_name" => null,
                "name_middle" => "Raymond",
                "spriden_last_name" => "Dusing",
                "name_suffix" => " ",
                "birth_date" => "1994-09-06",
                "email_add" => "dusingar@miamioh.edu",
                "gzbausr_hide_middle_name" => 'Y',
                "spbpers_sex" => "M",
                "privacy_status" => "N",
                "rnum" => "1"
            ]);

        $statement
            ->expects($this->at(2))
            ->method('fetchrow_assoc')
            ->willReturn(false);

        $this->request->method('getOptions')->willReturn([
            'pidm' => ['1440394']
        ]);

        $response = $this->personService->getPersons();

        $this->assertEquals($response->getPayload()[0]['ssn'],'00000000');
    }

    public function testCanGetSensitiveInformationByPersonId():void
    {
        $statement = $this->createMock(\MiamiOH\RESTng\Legacy\DB\STH\OCI8::class);
        $this->dbh->method('prepare')->willReturn($statement);
        $this->apiUser->method('isAuthenticated')->willReturn(true);
        $this->apiUser->expects($this->at(4))->method('isAuthorized')->with($this->equalTo('WebServices'),$this->equalTo('Person'),
        $this->equalTo('viewAllWithSsn'))->willReturn(true);
        $statement->method('execute')->willReturnSelf();
        $statement
            ->expects($this->at(1))
            ->method('fetchrow_assoc')
            ->willReturn([
                "szbuniq_banner_id" => "+01422366",
                "szbuniq_unique_id" => "DUSINGAR",
                "spriden_pidm" => "1440396",
                "spbpers_ssn" => "00000000",
                "name_prefix" => " ",
                "spriden_first_name" => "Andrew",
                "spbpers_pref_first_name" => null,
                "name_middle" => "Raymond",
                "spriden_last_name" => "Dusing",
                "name_suffix" => " ",
                "birth_date" => "1994-09-06",
                "email_add" => "dusingar@miamioh.edu",
                "gzbausr_hide_middle_name" => 'Y',
                "spbpers_sex" => "M",
                "privacy_status" => "N",
                "rnum" => "1"
            ]);

        $statement
            ->expects($this->at(2))
            ->method('fetchrow_assoc')
            ->willReturn(false);

        $this->request->method('getResourceParam')->with($this->equalTo('personID'))->willReturn(
          '1440394'
        );
        $this->request->method('getResourceParamKey')->with($this->equalTo('personID'))->willReturn(
            '1440394'
          );

        $response = $this->personService->getPersonsID();
        $this->assertEquals($response->getPayload()['ssn'],'00000000');
    }

    public function testCanGetSensitiveInformationByPersonIdAndPrivacyFlagF():void
    {
        $statement = $this->createMock(\MiamiOH\RESTng\Legacy\DB\STH\OCI8::class);
        $this->dbh->method('prepare')->willReturn($statement);
        $this->apiUser->method('isAuthenticated')->willReturn(true);
        $this->apiUser->expects($this->at(4))->method('isAuthorized')->with($this->equalTo('WebServices'),$this->equalTo('Person'),
        $this->equalTo('viewAllWithSsn'))->willReturn(true);
        $statement->method('execute')->willReturnSelf();
        $statement
            ->expects($this->at(1))
            ->method('fetchrow_assoc')
            ->willReturn([
                "szbuniq_banner_id" => "+01422366",
                "szbuniq_unique_id" => "DUSINGAR",
                "spriden_pidm" => "1440396",
                "spbpers_ssn" => "00000000",
                "name_prefix" => " ",
                "spriden_first_name" => "Andrew",
                "spbpers_pref_first_name" => null,
                "name_middle" => "Raymond",
                "spriden_last_name" => "Dusing",
                "name_suffix" => " ",
                "birth_date" => "1994-09-06",
                "email_add" => "dusingar@miamioh.edu",
                "gzbausr_hide_middle_name" => 'Y',
                "spbpers_sex" => "M",
                "privacy_status" => "F",
                "rnum" => "1"
            ]);

        $statement
            ->expects($this->at(2))
            ->method('fetchrow_assoc')
            ->willReturn(false);

        $this->request->method('getResourceParam')->with($this->equalTo('personID'))->willReturn(
          '1440394'
        );
        $this->request->method('getResourceParamKey')->with($this->equalTo('personID'))->willReturn(
            '1440394'
          );

        $response = $this->personService->getPersonsID();
        $this->assertEquals($response->getPayload()['ssn'],'00000000');
    } 

    public function testCanGetSensitiveInformationAndPrivacyFlagF():void
    {
        $statement = $this->createMock(\MiamiOH\RESTng\Legacy\DB\STH\OCI8::class);
        $this->dbh->method('prepare')->willReturn($statement);
        $this->apiUser->method('isAuthenticated')->willReturn(true);
        $this->apiUser->expects($this->at(4))->method('isAuthorized')->with($this->equalTo('WebServices'),$this->equalTo('Person'),
        $this->equalTo('viewAllWithSsn'))->willReturn(true);
        $statement->method('execute')->willReturnSelf();
        $statement
            ->expects($this->at(1))
            ->method('fetchrow_assoc')
            ->willReturn([
                "szbuniq_banner_id" => "+01422366",
                "szbuniq_unique_id" => "DUSINGAR",
                "spriden_pidm" => "1440396",
                "spbpers_ssn" => "00000000",
                "name_prefix" => " ",
                "spriden_first_name" => "Andrew",
                "spbpers_pref_first_name" => null,
                "name_middle" => "Raymond",
                "spriden_last_name" => "Dusing",
                "name_suffix" => " ",
                "birth_date" => "1994-09-06",
                "email_add" => "dusingar@miamioh.edu",
                "gzbausr_hide_middle_name" => 'Y',
                "spbpers_sex" => "M",
                "privacy_status" => "F",
                "rnum" => "1"
            ]);

        $statement
            ->expects($this->at(2))
            ->method('fetchrow_assoc')
            ->willReturn(false);

        $this->request->method('getOptions')->willReturn([
            'pidm' => ['1440394']
        ]);

        $response = $this->personService->getPersons();
        $this->assertEquals($response->getPayload()[0]['ssn'],'00000000');
    }

    public function testResidencyPopulationReturnsField(): void {
        $statement = $this->createMock(\MiamiOH\RESTng\Legacy\DB\STH\OCI8::class);
        $this->dbh->method('prepare')->willReturn($statement);
        $statement->method('execute')->willReturnSelf();
        $statement
            ->expects($this->at(1))
            ->method('fetchrow_assoc')
            ->willReturn([
                'szbuniq_banner_id' => '+01422366',
                'szbuniq_unique_id' => 'DUSINGAR',
                'spriden_pidm' => '1440396',
                'spbpers_ssn' => '00000000',
                'name_prefix' => ' ',
                'spriden_first_name' => 'Andrew',
                'spbpers_pref_first_name' => null,
                'name_middle' => 'Raymond',
                'spriden_last_name' => 'Dusing',
                'name_suffix' => ' ',
                'birth_date' => '1994-09-06',
                'email_add' => 'dusingar@miamioh.edu',
                'gzbausr_hide_middle_name' => null,
                'spbpers_sex' => 'M',
                'privacy_status' => 'N',
                'rnum' => '1',
                'student_resident_code' => 'R',
                'student_type_code' => 'U'
            ]);

        $this->request->method('getOptions')->willReturn([
            'population' => 'currentActiveResidentStudents'
        ]);

        $response = $this->personService->getPersons();
        $this->assertArrayHasKey('residencyStatus', $response->getPayload()[0]);
        $this->assertEquals('R', $response->getPayload()[0]['residencyStatus']);
    }

    public function testLimitResponseByTermCode(): void {
        $statement = $this->createMock(\MiamiOH\RESTng\Legacy\DB\STH\OCI8::class);
        $this->dbh->method('prepare')->willReturn($statement);
        $statement->method('execute')->willReturnSelf();
        $statement
            ->expects($this->at(1))
            ->method('fetchrow_assoc')
            ->willReturn([]);

        $this->request->method('getOptions')->willReturn([
            'population' => 'currentActiveResidentStudents',
            'effectiveTermCode' => 202020,
            'limit' => 1
        ]);

        $response = $this->personService->getPersons();
        $this->assertCount(0, $response->getPayload());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->request = $this->createMock(Request::class);
        $this->dbh = $this->createMock(DBH::class);
        $this->apiUser = $this->createMock(User::class);

        $databaseFactory = $this->createMock(DatabaseFactory::class);
        $databaseFactory->method('getHandle')->willReturn($this->dbh);

        $this->personService = new Person();
        $this->personService->setDatabase($databaseFactory);
        $this->personService->setRequest($this->request);
        $this->personService->setApiUser($this->apiUser);
    }

}