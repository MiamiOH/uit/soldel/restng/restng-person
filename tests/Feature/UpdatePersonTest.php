<?php
/**
 * Author: liaom
 * Date: 7/31/18
 * Time: 11:09 AM
 */

namespace MiamiOH\RestngPersonWebService\Tests\Feature;

use MiamiOH\RESTng\App;

class UpdatePersonTest extends TestCase
{
    public function testUnauthorized()
    {
        $this->credentialValidator->method('validateToken')->willReturn([
            'valid' => true,
            'username' => 'testuser',
        ]);

        $this->authorizationValidator
            ->method('validateAuthorizationForKey')
            ->willReturn(false);

        $response = $this->putJson('/person/v3?uniqueId=testuser&token=asdfasdf');

        $response->assertStatus(App::API_UNAUTHORIZED);
    }

    public function testMissingUniqueId()
    {
        $this->credentialValidator->method('validateToken')->willReturn([
            'valid' => true,
            'username' => 'testuser',
        ]);

        $this->authorizationValidator
            ->method('validateAuthorizationForKey')
            ->willReturn(true);

        $response = $this->putJson('/person/v3?token=asdfasdf');

        $response->assertStatus(App::API_FAILED);
    }

    public function testInvalidPreferredFirstName()
    {
        $this->credentialValidator->method('validateToken')->willReturn([
            'valid' => true,
            'username' => 'testuser',
        ]);

        $this->authorizationValidator
            ->method('validateAuthorizationForKey')
            ->willReturn(true);

        $response = $this->putJson('/person/v3?token=asdfasdf&uniqueId=liaom&preferredFirstName='.random_int(0,1000) );

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidDirectoryListing()
    {
        $this->credentialValidator->method('validateToken')->willReturn([
            'valid' => true,
            'username' => 'testuser',
        ]);

        $this->authorizationValidator
            ->method('validateAuthorizationForKey')
            ->willReturn(true);

        $response = $this->putJson('/person/v3?token=asdfasdf&uniqueId=liaom&directoryListing=C');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidHideMiddleName()
    {
        $this->credentialValidator->method('validateToken')->willReturn([
            'valid' => true,
            'username' => 'testuser',
        ]);

        $this->authorizationValidator
            ->method('validateAuthorizationForKey')
            ->willReturn(true);

        $response = $this->putJson('/person/v3?token=asdfasdf&uniqueId=liaom&hideMiddleName=Y');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testRemovePreferredName()
    {
        $this->credentialValidator->method('validateToken')->willReturn([
            'valid' => true,
            'username' => 'testuser',
        ]);

        $this->authorizationValidator
            ->method('validateAuthorizationForKey')
            ->willReturn(true);

        $this->dbh->method('queryall_array')
            ->willReturn([
                [
                    'szbuniq_pidm' => 123456
                ]
            ]);

        $this->dbh
            ->method('perform')
            ->with(
                $this->anything(),
                [
                    null,
                    '123456'
                ]
            )
            ->willReturn(true);

        $response = $this->putJson('/person/v3?token=asdfasdf&uniqueId=liaom&preferredFirstName=');

        $response->assertStatus(App::API_OK);
    }

    public function testPersonNotFoundByUniqueId()
    {
        $this->credentialValidator->method('validateToken')->willReturn([
            'valid' => true,
            'username' => 'testuser',
        ]);

        $this->authorizationValidator
            ->method('validateAuthorizationForKey')
            ->willReturn(true);

        $this->dbh->method('queryall_array')
            ->willReturn([]);

        $response = $this->putJson('/person/v3?token=asdfasdf&uniqueId=liaom');

        $response->assertStatus(App::API_BADREQUEST);
    }
}