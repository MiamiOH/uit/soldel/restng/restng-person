<?php
/**
 * Author: liaom
 * Date: 7/30/18
 * Time: 12:07 PM
 */

namespace MiamiOH\RestngPersonWebService\Tests\Feature;

class GetByMajorTest extends TestCase
{
    public function testGetByMajor()
    {
        $statement = $this->createMock(\MiamiOH\RESTng\Legacy\DB\STH\OCI8::class);
        $this->dbh->method('prepare')->willReturn($statement);
        $this->dbh->method('queryall_array')->willReturn([
            [
                'sgbstdn_pidm' => '1440396'
            ]
        ]);
        $statement->method('execute')->willReturnSelf();
        $statement
            ->expects($this->at(1))
            ->method('fetchrow_assoc')
            ->willReturn([
                "szbuniq_banner_id" => "+01422366",
                "szbuniq_unique_id" => "DUSINGAR",
                "spriden_pidm" => "1440396",
                "spbpers_ssn" => "00000000",
                "name_prefix" => " ",
                "spriden_first_name" => "Andrew",
                "spbpers_pref_first_name" => null,
                "name_middle" => "Raymond",
                "spriden_last_name" => "Dusing",
                "name_suffix" => " ",
                "birth_date" => "1994-09-06",
                "email_add" => "dusingar@miamioh.edu",
                "gzbausr_hide_middle_name" => 'Y',
                "spbpers_sex" => "M",
                "privacy_status" => "N",
                "rnum" => "1"
            ]);

        $statement
            ->expects($this->at(2))
            ->method('fetchrow_assoc')
            ->willReturn(false);

        $response = $this->getJson('/person/v3/major?majorCodes=apet');

        $response->assertJson([
            'data' => [
                [
                    "uniqueId" => "DUSINGAR",
                    "prefix" => " ",
                    "suffix" => " ",
                    "givenNameLegal" => "Andrew",
                    "givenNamePreferred" => "Andrew",
                    "familyName" => "Dusing",
                    "middleName" => "Raymond",
                    "hideMiddleName" => true,
                    "informalDisplayedName" => "Andrew Raymond Dusing",
                    "formalDisplayedName" => " Andrew Raymond Dusing",
                    "informalSortedName" => "Dusing, Andrew Raymond",
                    "formalSortedName" => "Dusing, Andrew Raymond "
                ]
            ]
        ]);
    }

    public function testNoMajorCode()
    {
        $response = $this->getJson('/person/v3/major');
        $response->assertJson([
            'data' => []
        ]);
    }
}