grant select on aprcatg to MUWS_SEC_RL;
grant select on shrdgmr to MUWS_SEC_RL;
grant select on atvdonr to MUWS_SEC_RL;
grant select on gzbausr to muws_sec_rl;
grant select, insert, update on spbpers to muws_sec_rl;
grant select, insert, update on goradid to muws_sec_rl;
grant select, update on gzbausr to muws_sec_rl;
grant select, update on gzragst to muws_sec_rl;
grant select, insert, update on spriden to muws_sec_rl;
